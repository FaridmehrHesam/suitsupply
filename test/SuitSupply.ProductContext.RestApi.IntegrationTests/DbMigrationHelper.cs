﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SuitSupply.ProductContext.Domain.Products;
using SuitSupply.ProductContext.Domain.Products.ValueObjects;
using SuitSupply.ProductContext.Persistence;

namespace SuitSupply.ProductContext.RestApi.IntegrationTests
{
    public static class DbMigrationHelper
    {
        public static void EnsureSeedData(IServiceProvider serviceProvider)
        {
            var dbContext = serviceProvider.GetRequiredService<ProductWriteDbContext>();

            dbContext.Database.Migrate();

            SeedProduct(dbContext);

            dbContext.SaveChanges();
        }

        private static void SeedProduct(ProductWriteDbContext dbContext)
        {
            var product = Product.Create(
                DbMigrationConstants.ProductCode,
                DbMigrationConstants.ProductName,
                new Price(DbMigrationConstants.ProductPrice));

            dbContext.Products.Add(product);
        }
    }
}