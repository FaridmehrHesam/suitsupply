﻿using SuitSupply.ProductContext.Domain.Products;

namespace SuitSupply.ProductContext.RestApi.IntegrationTests
{
    public static class DbMigrationConstants
    {
        public const string ProductCode = nameof(Product.Code);

        public const string ProductName = nameof(Product.Name);

        public const double ProductPrice = 100;
    }
}