﻿using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using FluentAssertions;

namespace SuitSupply.ProductContext.RestApi.IntegrationTests.v1_0.Products.Controllers
{
    public partial class ProductsControllerIntegrationTest
    {
        private async Task<byte[]> GetPhoto(string productCode, HttpStatusCode responseStatusCode = HttpStatusCode.OK)
        {
            var response = await client.GetAsync($"{ProductsUrl}/{productCode}/photo").ConfigureAwait(false);

            response.StatusCode.Should().Be(responseStatusCode);

            return responseStatusCode != HttpStatusCode.OK
                ? null
                : await response.Content.ReadAsByteArrayAsync();
        }

        private async Task UploadPhoto(string productCode, byte[] data,
            HttpStatusCode responseStatusCode = HttpStatusCode.OK)
        {
            HttpResponseMessage response;
            var url = $"{ProductsUrl}/{productCode}/photo";

            if (data == null)
                response = await client.PostAsync(url, null);
            else
                using (var memoryStream = new MemoryStream(data))
                using (var streamContent = new StreamContent(memoryStream))
                using (var formData = new MultipartFormDataContent())
                {
                    formData.Add(streamContent, "photo", "image.jpg");
                    formData.First().Headers.ContentType = MediaTypeHeaderValue.Parse("image/jpeg");
                    response = await client.PostAsync(url, formData);
                }

            response.StatusCode.Should().Be(responseStatusCode);
        }
    }
}