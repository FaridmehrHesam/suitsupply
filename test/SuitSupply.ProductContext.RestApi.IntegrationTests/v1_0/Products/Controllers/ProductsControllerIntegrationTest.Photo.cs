﻿using System.Net;
using System.Threading.Tasks;
using Faker;
using FluentAssertions;
using SuitSupply.ProductContext.RestApi.IntegrationTests.Properties;
using Xunit;

namespace SuitSupply.ProductContext.RestApi.IntegrationTests.v1_0.Products.Controllers
{
    public partial class ProductsControllerIntegrationTest
    {
        [Theory]
        [InlineData(null)]
        [InlineData(new byte[0])]
        public async Task TestUploadPhoto_WhenPhotoIsEmpty_StatusMustBeBadRequest(byte[] value)
        {
            var model = new ProductWriteModelBuilder().Build();
            var product = await Create(model);

            await UploadPhoto(product.Code, value, HttpStatusCode.BadRequest);

            await Delete(product.Code);
        }

        [Fact]
        public async Task TestGetPhoto_WhenCodeDoesNotExist_StatusMustBeNotFound()
        {
            await GetPhoto(Name.First(), HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestGetPhoto_WhenEverythingIsOK_StatusMustBeOk()
        {
            var model = new ProductWriteModelBuilder().Build();
            var product = await Create(model);

            await UploadPhoto(product.Code, Resources.SuitSupply);
            var photo = await GetPhoto(product.Code);

            photo.Should().BeEquivalentTo(Resources.SuitSupply);

            await Delete(product.Code);
        }

        [Fact]
        public async Task TestGetPhoto_WhenPhotoDoesNotExist_StatusMustBeNotFound()
        {
            var model = new ProductWriteModelBuilder().Build();
            var product = await Create(model);

            await GetPhoto(product.Code, HttpStatusCode.NotFound);

            await Delete(product.Code);
        }

        [Fact]
        public async Task TestUploadPhoto_WhenCodeDoesNotExist_StatusMustBeNotFound()
        {
            await UploadPhoto(Name.First(), Resources.SuitSupply, HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestUploadPhoto_WhenEverythingIsOK_StatusMustBeOk()
        {
            var model = new ProductWriteModelBuilder().Build();
            var createdProduct = await Create(model);

            await UploadPhoto(createdProduct.Code, Resources.SuitSupply);
            var updatedProduct = await GetByCode(createdProduct.Code);

            updatedProduct.Photo.Should().Be(options.PhotoBaseUrl.Replace("${0}", createdProduct.Code));

            await Delete(createdProduct.Code);
        }

        [Fact]
        public async Task TestUploadPhoto_WhenPhotoAlreadyUploaded_StatusMustBeOk()
        {
            var model = new ProductWriteModelBuilder().Build();
            var product = await Create(model);

            await UploadPhoto(product.Code, Resources.SuitSupply);
            await UploadPhoto(product.Code, Resources.Image);

            var photo = await GetPhoto(product.Code);

            photo.Should().BeEquivalentTo(Resources.Image);

            await Delete(product.Code);
        }
    }
}