﻿using Faker;
using SuitSupply.ProductContext.Facade.v1_0.Products.Models;

namespace SuitSupply.ProductContext.RestApi.IntegrationTests.v1_0.Products.Controllers
{
    public class ProductWriteModelBuilder
    {
        private string code;

        private string name;

        private double price;

        public ProductWriteModelBuilder()
        {
            code = Name.First();
            name = Name.FullName();
            price = RandomNumber.Next(1, 999);
        }

        public ProductWriteModel Build()
        {
            return new ProductWriteModel
            {
                Code = code,
                Name = name,
                Price = price
            };
        }

        public ProductWriteModelBuilder WithCode(string newCode)
        {
            code = newCode;
            return this;
        }

        public ProductWriteModelBuilder WithName(string newName)
        {
            name = newName;
            return this;
        }

        public ProductWriteModelBuilder WithPrice(double newPrice)
        {
            price = newPrice;
            return this;
        }
    }
}