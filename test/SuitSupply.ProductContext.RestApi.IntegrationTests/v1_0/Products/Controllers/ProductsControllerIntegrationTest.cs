﻿using System;
using System.Net;
using System.Threading.Tasks;
using Faker;
using FluentAssertions;
using Xunit;

namespace SuitSupply.ProductContext.RestApi.IntegrationTests.v1_0.Products.Controllers
{
    [Collection(nameof(TestFixtureCollection))]
    public partial class ProductsControllerIntegrationTest
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public async Task TestPost_WhenCodeIsEmpty_StatusMustBeBadRequest(string value)
        {
            var model = new ProductWriteModelBuilder()
                .WithCode(value)
                .Build();

            await Create(model, HttpStatusCode.BadRequest);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public async Task TestPost_WhenNameIsEmpty_StatusMustBeBadRequest(string value)
        {
            var model = new ProductWriteModelBuilder()
                .WithName(value)
                .Build();

            await Create(model, HttpStatusCode.BadRequest);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-1000)]
        public async Task TestPost_WhenPriceIsLessThanZero_StatusMustBeBadRequest(double value)
        {
            var model = new ProductWriteModelBuilder()
                .WithPrice(value)
                .Build();

            await Create(model, HttpStatusCode.BadRequest);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public async Task TestPut_WhenCodeIsEmpty_StatusMustBeBadRequest(string value)
        {
            var createModel = new ProductWriteModelBuilder().Build();
            var updateModel = new ProductWriteModelBuilder().WithCode(value).Build();
            var product = await Create(createModel);

            await Update(product.Code, updateModel, HttpStatusCode.BadRequest);
            await Delete(product.Code);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public async Task TestPut_WhenNameIsEmpty_StatusMustBeBadRequest(string value)
        {
            var createModel = new ProductWriteModelBuilder().Build();
            var updateModel = new ProductWriteModelBuilder().WithName(value).Build();
            var product = await Create(createModel);

            await Update(product.Code, updateModel, HttpStatusCode.BadRequest);
            await Delete(product.Code);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-1000)]
        public async Task TestPut_WhenPriceIsLessThanZero_StatusMustBeBadRequest(double value)
        {
            var createModel = new ProductWriteModelBuilder().Build();
            var updateModel = new ProductWriteModelBuilder().WithPrice(value).Build();
            var product = await Create(createModel);

            await Update(product.Code, updateModel, HttpStatusCode.BadRequest);
            await Delete(product.Code);
        }

        [Fact]
        public async Task TestConfirmPrice_WhenAlreadyConfirmed_StatusMustBeBadRequest()
        {
            var model = new ProductWriteModelBuilder()
                .WithPrice(RandomNumber.Next(1000, int.MaxValue))
                .Build();
            var product = await Create(model);

            await ConfirmPrice(product.Code);
            await ConfirmPrice(product.Code, HttpStatusCode.BadRequest);

            await Delete(model.Code);
        }

        [Fact]
        public async Task TestConfirmPrice_WhenCodeDoesNotExist_StatusMustBeNotFound()
        {
            await ConfirmPrice(Name.First(), HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestConfirmPrice_WhenEverythingIsOk_StatusMustBeOk()
        {
            var model = new ProductWriteModelBuilder()
                .WithPrice(RandomNumber.Next(1000, int.MaxValue))
                .Build();
            var product = await Create(model);

            await ConfirmPrice(product.Code);

            var updatedProduct = await GetByCode(product.Code);

            updatedProduct.IsConfirmed.Should().BeTrue();
            updatedProduct.LastUpdated.Should().BeAfter(product.LastUpdated);

            await Delete(updatedProduct.Code);
        }

        [Fact]
        public async Task TestConfirmPrice_WhenPriceIsLessThan999_StatusMustBeBadRequest()
        {
            var model = new ProductWriteModelBuilder()
                .WithPrice(RandomNumber.Next(1, 999))
                .Build();
            var product = await Create(model);

            await ConfirmPrice(product.Code, HttpStatusCode.BadRequest);

            await Delete(model.Code);
        }

        [Fact]
        public async Task TestDelete_WhenCodeDoesNotExist_StatusMustBeNotFound()
        {
            await Delete(Name.First(), HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestDelete_WhenEverythingIsOk_StatusMustBeOk()
        {
            var model = new ProductWriteModelBuilder().Build();
            var product = await Create(model).ConfigureAwait(false);

            await Delete(product.Code).ConfigureAwait(false);
        }

        [Fact]
        public async Task TestExport_WhenEverythingIsOk_StatusMustBeOk()
        {
            var products = await GetAll();
            var result = await Export();
            var expected = csvExporter.Export(products);

            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public async Task TestExport_WhenUsingSearch_StatusMustBeOk()
        {
            var model = new ProductWriteModelBuilder().Build();
            var product = await Create(model).ConfigureAwait(false);

            var products = await GetAll(product.Code);
            var result = await Export(product.Code);
            var expected = csvExporter.Export(products);

            result.Should().BeEquivalentTo(expected);

            await Delete(product.Code).ConfigureAwait(false);
        }

        [Fact]
        public async Task TestGetAll_WhenEverythingIsOk_StatusMustBeOk()
        {
            var products = await GetAll();

            products.Should().HaveCount(1);

            products[0].Code.Should().Be(DbMigrationConstants.ProductCode);
            products[0].Name.Should().Be(DbMigrationConstants.ProductName);
            products[0].Price.Should().Be(DbMigrationConstants.ProductPrice);
            products[0].IsConfirmed.Should().BeTrue();
            products[0].LastUpdated.Should().BeAfter(DateTime.MinValue);
        }

        [Fact]
        public async Task TestGetAll_WhenUsingSearch_StatusMustBeOk()
        {
            var model = new ProductWriteModelBuilder().Build();
            var product = await Create(model).ConfigureAwait(false);
            var products = await GetAll(product.Code);

            products.Should().HaveCount(1);

            products[0].Code.Should().Be(product.Code);
            products[0].Name.Should().Be(product.Name);
            products[0].Price.Should().Be(product.Price);
            products[0].IsConfirmed.Should().Be(product.IsConfirmed);
            products[0].LastUpdated.Should().Be(product.LastUpdated);

            await Delete(product.Code).ConfigureAwait(false);
        }

        [Fact]
        public async Task TestGetByCode_WhenCodeDoesNotExist_StatusMustBeNotFound()
        {
            await GetByCode(Name.First(), HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task TestGetByCode_WhenEverythingIsOk_StatusMustBeOk()
        {
            var product = await GetByCode(DbMigrationConstants.ProductCode);

            product.Code.Should().Be(DbMigrationConstants.ProductCode);
            product.Name.Should().Be(DbMigrationConstants.ProductName);
            product.Price.Should().Be(DbMigrationConstants.ProductPrice);
            product.IsConfirmed.Should().BeTrue();
            product.LastUpdated.Should().BeAfter(DateTime.MinValue);
        }

        [Fact]
        public async Task TestGetByCode_WhenUsingCaseInsensitive_StatusMustBeOk()
        {
            var product = await GetByCode(DbMigrationConstants.ProductCode.ToUpper());

            product.Code.Should().Be(DbMigrationConstants.ProductCode);
            product.Name.Should().Be(DbMigrationConstants.ProductName);
            product.Price.Should().Be(DbMigrationConstants.ProductPrice);
            product.IsConfirmed.Should().BeTrue();
            product.LastUpdated.Should().BeAfter(DateTime.MinValue);
        }

        [Fact]
        public async Task TestPost_WhenEverythingIsOkAndPriceDoesNotNeedConfirmation_StatusMustBeCreated()
        {
            var model = new ProductWriteModelBuilder()
                .WithCode(Name.First())
                .WithName(Name.FullName())
                .WithPrice(RandomNumber.Next(1, 999))
                .Build();
            var product = await Create(model);

            product.Code.Should().Be(model.Code);
            product.Name.Should().Be(model.Name);
            product.Price.Should().Be(model.Price);
            product.IsConfirmed.Should().BeTrue();
            product.LastUpdated.Should().BeAfter(DateTime.MinValue);

            await Delete(product.Code);
        }

        [Fact]
        public async Task TestPost_WhenEverythingIsOkAndPriceNeedsConfirmation_StatusMustBeCreated()
        {
            var model = new ProductWriteModelBuilder()
                .WithCode(Name.First())
                .WithName(Name.FullName())
                .WithPrice(RandomNumber.Next(1000, int.MaxValue))
                .Build();
            var product = await Create(model);

            product.Code.Should().Be(model.Code);
            product.Name.Should().Be(model.Name);
            product.Price.Should().Be(model.Price);
            product.IsConfirmed.Should().BeFalse();
            product.LastUpdated.Should().BeAfter(DateTime.MinValue);

            await Delete(product.Code);
        }

        [Fact]
        public async Task TestPut_WhenEverythingIsOk_StatusMustBeNoContent()
        {
            var createModel = new ProductWriteModelBuilder().Build();
            var updateModel = new ProductWriteModelBuilder()
                .WithCode(Name.First())
                .WithName(Name.FullName())
                .WithPrice(RandomNumber.Next(1, 999))
                .Build();
            var createdProduct = await Create(createModel);

            await Update(createdProduct.Code, updateModel);

            var updatedProduct = await GetByCode(updateModel.Code);

            updatedProduct.Code.Should().Be(updateModel.Code);
            updatedProduct.Name.Should().Be(updateModel.Name);
            updatedProduct.Price.Should().Be(updateModel.Price);
            updatedProduct.IsConfirmed.Should().BeTrue();
            updatedProduct.LastUpdated.Should().BeAfter(createdProduct.LastUpdated);

            await Delete(updatedProduct.Code);
        }

        [Fact]
        public async Task TestPut_WhenNewPriceDoesNotNeedConfirmation_StatusMustBeNoContent()
        {
            var createModel = new ProductWriteModelBuilder()
                .WithPrice(RandomNumber.Next(1000, int.MaxValue))
                .Build();
            var updateModel = new ProductWriteModelBuilder()
                .WithPrice(RandomNumber.Next(1, 999))
                .Build();
            var createdProduct = await Create(createModel);

            await Update(createdProduct.Code, updateModel);

            var updatedProduct = await GetByCode(updateModel.Code);

            updatedProduct.IsConfirmed.Should().BeTrue();
            updatedProduct.LastUpdated.Should().BeAfter(createdProduct.LastUpdated);

            await Delete(updatedProduct.Code);
        }

        [Fact]
        public async Task TestPut_WhenNewPriceNeedsConfirmation_StatusMustBeNoContent()
        {
            var createModel = new ProductWriteModelBuilder()
                .WithPrice(RandomNumber.Next(1, 999))
                .Build();
            var updateModel = new ProductWriteModelBuilder()
                .WithPrice(RandomNumber.Next(1000, int.MaxValue))
                .Build();
            var createdProduct = await Create(createModel);

            await Update(createdProduct.Code, updateModel);

            var updatedProduct = await GetByCode(updateModel.Code);

            updatedProduct.IsConfirmed.Should().BeFalse();
            updatedProduct.LastUpdated.Should().BeAfter(createdProduct.LastUpdated);

            await Delete(updatedProduct.Code);
        }
    }
}