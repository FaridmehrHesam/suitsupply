﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using SuitSupply.Framework.RestApi;
using SuitSupply.ProductContext.Facade.v1_0.Products.Models;
using SuitSupply.ProductContext.RestApi.Services;
using SuitSupply.ProductContext.RestApi.v1_0;

namespace SuitSupply.ProductContext.RestApi.IntegrationTests.v1_0.Products.Controllers
{
    public partial class ProductsControllerIntegrationTest
    {
        private const string ProductsUrl = "/rest/api/product/v1.0/Products";

        private readonly HttpClient client;

        private readonly ICsvExporter csvExporter;

        private readonly ProductOptions options;

        public ProductsControllerIntegrationTest(TestFixture testFixture)
        {
            client = testFixture.Client;
            options = testFixture.ProductOptions;
            csvExporter = testFixture.CsvExporter;
        }

        private async Task<ProductReadModel> Create(
            ProductWriteModel model,
            HttpStatusCode responseStatusCode = HttpStatusCode.Created)
        {
            var response = await client
                .PostAsJsonAsync(ProductsUrl, model)
                .ConfigureAwait(false);

            response.StatusCode.Should().Be(responseStatusCode);

            return responseStatusCode != HttpStatusCode.Created
                ? null
                : (await response.Content.ReadAsAsync<ResponseModel<ProductReadModel>>()).Values;
        }

        private async Task<ProductReadModel> GetByCode(
            string productCode,
            HttpStatusCode responseStatusCode = HttpStatusCode.OK)
        {
            var response = await client.GetAsync($"{ProductsUrl}/{productCode}").ConfigureAwait(false);

            response.StatusCode.Should().Be(responseStatusCode);

            return responseStatusCode != HttpStatusCode.OK
                ? null
                : (await response.Content.ReadAsAsync<ResponseModel<ProductReadModel>>()).Values;
        }

        private async Task<ProductReadModel[]> GetAll(
            string search = "",
            HttpStatusCode responseStatusCode = HttpStatusCode.OK)
        {
            var response = await client.GetAsync($"{ProductsUrl}?search={search}").ConfigureAwait(false);

            response.StatusCode.Should().Be(responseStatusCode);

            return responseStatusCode != HttpStatusCode.OK
                ? null
                : (await response.Content.ReadAsAsync<ResponseModel<ProductReadModel[]>>()).Values;
        }

        private async Task<byte[]> Export(string search = "", HttpStatusCode responseStatusCode = HttpStatusCode.OK)
        {
            var response = await client.GetAsync($"{ProductsUrl}/export?search={search}").ConfigureAwait(false);

            response.StatusCode.Should().Be(responseStatusCode);

            return responseStatusCode != HttpStatusCode.OK
                ? null
                : await response.Content.ReadAsByteArrayAsync();
        }

        private async Task Update(
            string productCode,
            ProductWriteModel model,
            HttpStatusCode responseStatusCode = HttpStatusCode.NoContent)
        {
            var response = await client
                .PutAsJsonAsync($"{ProductsUrl}/{productCode}", model)
                .ConfigureAwait(false);

            response.StatusCode.Should().Be(responseStatusCode);
        }

        private async Task ConfirmPrice(string productCode, HttpStatusCode responseStatusCode = HttpStatusCode.OK)
        {
            var response = await client
                .PostAsJsonAsync($"{ProductsUrl}/{productCode}/confirmPrice", new { })
                .ConfigureAwait(false);

            response.StatusCode.Should().Be(responseStatusCode);
        }

        private async Task Delete(string productCode, HttpStatusCode responseStatusCode = HttpStatusCode.OK)
        {
            var response = await client.DeleteAsync($"{ProductsUrl}/{productCode}").ConfigureAwait(false);

            response.StatusCode.Should().Be(responseStatusCode);
        }
    }
}