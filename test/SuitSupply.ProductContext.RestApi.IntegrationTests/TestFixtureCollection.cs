﻿using Xunit;

namespace SuitSupply.ProductContext.RestApi.IntegrationTests
{
    [CollectionDefinition(nameof(TestFixtureCollection))]
    public class TestFixtureCollection : ICollectionFixture<TestFixture>
    {
    }
}