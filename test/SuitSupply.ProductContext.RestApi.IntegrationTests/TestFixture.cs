﻿using System;
using System.IO;
using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using SuitSupply.ProductContext.Persistence;
using SuitSupply.ProductContext.RestApi.Host;
using SuitSupply.ProductContext.RestApi.Services;
using SuitSupply.ProductContext.RestApi.v1_0;

namespace SuitSupply.ProductContext.RestApi.IntegrationTests
{
    public class TestFixture : IDisposable
    {
        private const string AppSettingsFileName = "appsettings.json";

        private readonly TestServer testServer;

        public TestFixture()
        {
            var appSettingsFilePath = Path.Combine(AppContext.BaseDirectory, AppSettingsFileName);
            var appSettingsContent = File.ReadAllText(appSettingsFilePath);

            appSettingsContent = appSettingsContent.Replace("${dbName}", $"ITDB{DateTime.Now.Ticks}");
            File.WriteAllText(appSettingsFilePath, appSettingsContent);

            var configurationRoot = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile(AppSettingsFileName)
                .Build();

            var builder = new WebHostBuilder()
                .UseEnvironment("Production")
                .UseConfiguration(configurationRoot)
                .UseStartup<Startup>();

            testServer = new TestServer(builder);
            Client = testServer.CreateClient();
            ProductOptions = testServer.Host.Services.GetService<IOptions<ProductOptions>>().Value;
            CsvExporter = testServer.Host.Services.GetService<ICsvExporter>();

            DbMigrationHelper.EnsureSeedData(testServer.Host.Services);
        }

        public HttpClient Client { get; }

        public ProductOptions ProductOptions { get; }

        public ICsvExporter CsvExporter { get; }

        public void Dispose()
        {
            var appSettingsFilePath = Path.Combine(AppContext.BaseDirectory, AppSettingsFileName);
            var dbContext = testServer.Host.Services.GetService<ProductWriteDbContext>();

            dbContext.Database.EnsureDeleted();
            File.Delete(appSettingsFilePath);

            Client.Dispose();
            testServer.Dispose();
        }
    }
}