﻿using SuitSupply.ProductContext.Domain.Products;
using SuitSupply.ProductContext.Domain.Products.ValueObjects;
using SuitSupply.ProductContext.Domain.UnitTests.Products.ValueObjects;

namespace SuitSupply.ProductContext.Domain.UnitTests.Products
{
    public class ProductBuilder
    {
        private string code;

        private string name;

        private string photo;

        private Price price;

        public ProductBuilder()
        {
            code = nameof(Product.Code);
            name = nameof(Product.Name);
            photo = nameof(Product.Photo);
            price = new PriceBuilder().Build();
        }

        public Product Build()
        {
            var product = Product.Create(code, name, price);

            product.SetPhoto(photo);

            return product;
        }

        public ProductBuilder WithCode(string newCode)
        {
            code = newCode;
            return this;
        }

        public ProductBuilder WithName(string newName)
        {
            name = newName;
            return this;
        }

        public ProductBuilder WithPhoto(string newPhoto)
        {
            photo = newPhoto;
            return this;
        }

        public ProductBuilder WithPrice(Price newPrice)
        {
            price = newPrice;
            return this;
        }
    }
}