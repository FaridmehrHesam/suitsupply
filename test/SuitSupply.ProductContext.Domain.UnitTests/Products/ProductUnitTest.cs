﻿using System;
using System.Linq;
using FluentAssertions;
using SuitSupply.Framework.Domain.Exceptions;
using SuitSupply.ProductContext.Domain.Products.Events;
using SuitSupply.ProductContext.Domain.Properties;
using SuitSupply.ProductContext.Domain.UnitTests.Products.ValueObjects;
using Xunit;

namespace SuitSupply.ProductContext.Domain.UnitTests.Products
{
    public class ProductUnitTest
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void TestSetCode_WhenValueIsEmpty_ThrowsException(string value)
        {
            var action = new Action(() => new ProductBuilder().WithCode(value).Build());

            action.Should().Throw<DomainException>().WithMessage(Resources.Product_CodeCanNotBeEmpty);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void TestSetName_WhenValueIsEmpty_ThrowsException(string value)
        {
            var action = new Action(() => new ProductBuilder().WithName(value).Build());

            action.Should().Throw<DomainException>().WithMessage(Resources.Product_NameCanNotBeEmpty);
        }

        [Fact]
        public void TestConfirmPrice_WhenAlreadyConfirmed_ThrowsException()
        {
            var price = new PriceBuilder().WithValue(1000).Build();
            var product = new ProductBuilder().WithPrice(price).Build();
            var action = new Action(() => product.ConfirmPrice());

            product.ConfirmPrice();

            action.Should().Throw<DomainException>().WithMessage(Resources.Product_PriceCanBeConfirmedOnce);
        }

        [Fact]
        public void TestConfirmPrice_WhenEverythingIsOk_IsConfirmedMustBeTrue()
        {
            var price = new PriceBuilder().WithValue(1000).Build();
            var product = new ProductBuilder().WithPrice(price).Build();
            var oldLastUpdated = product.LastUpdated;

            product.ClearEvents();
            product.ConfirmPrice();

            var @event = product.DomainEvents.OfType<ProductConfirmedEvent>().First();

            product.DomainEvents.Should().HaveCount(1);
            product.IsConfirmed.Should().BeTrue();
            product.LastUpdated.Should().BeAfter(oldLastUpdated);
            @event.AggregateId.Should().Be(product.Id);
        }

        [Fact]
        public void TestCreate_WhenEverythingIsOk_PropertiesMustBeSet()
        {
            const string code = "NewCode";
            const string name = "NewName";
            const string photo = "NewPhoto";

            var price = new PriceBuilder().WithValue(10).Build();
            var product = new ProductBuilder()
                .WithCode(code)
                .WithName(name)
                .WithPrice(price)
                .WithPhoto(photo)
                .Build();

            var @event = product.DomainEvents.OfType<ProductCreatedEvent>().First();

            product.Code.Should().Be(code);
            product.Name.Should().Be(name);
            product.Photo.Should().Be(photo);
            product.Price.Should().Be(price);
            product.IsConfirmed.Should().BeTrue();
            product.LastUpdated.Should().BeAfter(DateTime.MinValue);
            @event.AggregateId.Should().Be(product.Id);
        }

        [Fact]
        public void TestCreate_WhenPriceIsHigherThan999_IsConfirmedMustBeFalse()
        {
            var price = new PriceBuilder().WithValue(1000).Build();
            var product = new ProductBuilder().WithPrice(price).Build();

            product.IsConfirmed.Should().BeFalse();
        }

        [Fact]
        public void TestCreate_WhenPriceIsLessThan999_IsConfirmedMustBeTrue()
        {
            var price = new PriceBuilder().WithValue(10).Build();
            var product = new ProductBuilder().WithPrice(price).Build();

            product.IsConfirmed.Should().BeTrue();
        }

        [Fact]
        public void TestDelete_WhenEverythingIsOk_MustBeDeleted()
        {
            var product = new ProductBuilder().Build();

            product.ClearEvents();
            product.Delete();

            var @event = product.DomainEvents.OfType<ProductDeletedEvent>().First();

            product.IsDeleted().Should().BeTrue();
            product.DomainEvents.Should().HaveCount(1);
            @event.AggregateId.Should().Be(product.Id);
        }

        [Fact]
        public void TestDelete_WhenMarkAsDeletedIsTrue_ThrowsException()
        {
            var product = new ProductBuilder().Build();
            var action = new Action(() => product.Delete());

            product.Delete();

            action.Should().Throw<InvalidOperationException>();
        }

        [Fact]
        public void TestSetCode_WhenEverythingIsOk_ValueMustBeSet()
        {
            const string code = "NewCode";

            var product = new ProductBuilder().Build();
            var oldCode = product.Code;
            var oldLastUpdated = product.LastUpdated;

            product.ClearEvents();
            product.SetCode(code);

            var @event = product.DomainEvents.OfType<ProductCodeChangedEvent>().First();

            product.DomainEvents.Should().HaveCount(1);
            product.Code.Should().Be(code);
            product.LastUpdated.Should().BeAfter(oldLastUpdated);
            @event.AggregateId.Should().Be(product.Id);
            @event.OldValue.Should().Be(oldCode);
            @event.NewValue.Should().Be(code);
        }

        [Fact]
        public void TestSetCode_WhenValueIsSame_LastUpdatedMustNotChange()
        {
            const string code = "NewCode";

            var product = new ProductBuilder().WithCode(code).Build();
            var lastUpdated = product.LastUpdated;

            product.ClearEvents();
            product.SetCode(code);

            product.DomainEvents.Should().HaveCount(0);
            product.LastUpdated.Should().Be(lastUpdated);
        }

        [Fact]
        public void TestSetName_WhenEverythingIsOk_ValueMustBeSet()
        {
            const string name = "NewCode";

            var product = new ProductBuilder().Build();
            var oldName = product.Name;
            var oldLastUpdated = product.LastUpdated;

            product.ClearEvents();
            product.SetName(name);

            var @event = product.DomainEvents.OfType<ProductNameChangedEvent>().First();

            product.DomainEvents.Should().HaveCount(1);
            product.Name.Should().Be(name);
            product.LastUpdated.Should().BeAfter(oldLastUpdated);
            @event.AggregateId.Should().Be(product.Id);
            @event.OldValue.Should().Be(oldName);
            @event.NewValue.Should().Be(name);
        }

        [Fact]
        public void TestSetName_WhenValueIsSame_LastUpdatedMustNotChange()
        {
            const string name = "NewName";

            var product = new ProductBuilder().WithName(name).Build();
            var lastUpdated = product.LastUpdated;

            product.ClearEvents();
            product.SetName(name);

            product.DomainEvents.Should().HaveCount(0);
            product.LastUpdated.Should().Be(lastUpdated);
        }

        [Fact]
        public void TestSetPhoto_WhenEverythingIsOk_ValueMustBeSet()
        {
            const string photo = "NewPhoto";

            var product = new ProductBuilder().Build();
            var oldLastUpdated = product.LastUpdated;

            product.ClearEvents();
            product.SetPhoto(photo);

            var @event = product.DomainEvents.OfType<ProductPhotoChangedEvent>().First();

            product.DomainEvents.Should().HaveCount(1);
            product.Photo.Should().Be(photo);
            product.LastUpdated.Should().BeAfter(oldLastUpdated);
            @event.AggregateId.Should().Be(product.Id);
        }

        [Fact]
        public void TestSetPhoto_WhenValueIsSame_LastUpdatedMustNotChange()
        {
            const string photo = "NewPhoto";

            var product = new ProductBuilder().WithPhoto(photo).Build();
            var lastUpdated = product.LastUpdated;

            product.ClearEvents();
            product.SetPhoto(photo);

            product.DomainEvents.Should().HaveCount(0);
            product.LastUpdated.Should().Be(lastUpdated);
        }

        [Fact]
        public void TestSetPrice_WhenEverythingIsOk_ValueMustBeSet()
        {
            var price = new PriceBuilder().WithValue(1000).Build();
            var product = new ProductBuilder().Build();
            var oldPrice = product.Price;
            var oldLastUpdated = product.LastUpdated;

            product.ClearEvents();
            product.SetPrice(price);

            var @event = product.DomainEvents.OfType<ProductPriceChangedEvent>().First();

            product.DomainEvents.Should().HaveCount(1);
            product.Price.Should().Be(price);
            product.LastUpdated.Should().BeAfter(oldLastUpdated);
            @event.AggregateId.Should().Be(product.Id);
            @event.OldValue.Should().Be(oldPrice.Value);
            @event.NewValue.Should().Be(price.Value);
        }

        [Fact]
        public void TestSetPrice_WhenValueIsHigherThan999_IsConfirmedMustBeFalse()
        {
            var priceBuilder = new PriceBuilder();
            var price = priceBuilder.WithValue(1000).Build();
            var product = new ProductBuilder().WithPrice(priceBuilder.WithValue(10).Build()).Build();
            var oldLastUpdated = product.LastUpdated;

            product.SetPrice(price);

            product.Price.Should().Be(price);
            product.IsConfirmed.Should().BeFalse();
            product.LastUpdated.Should().BeAfter(oldLastUpdated);
        }

        [Fact]
        public void TestSetPrice_WhenValueIsLessThan999_IsConfirmedMustBeTrue()
        {
            var priceBuilder = new PriceBuilder();
            var price = priceBuilder.WithValue(10).Build();
            var product = new ProductBuilder().WithPrice(priceBuilder.WithValue(1000).Build()).Build();
            var oldLastUpdated = product.LastUpdated;

            product.SetPrice(price);

            product.Price.Should().Be(price);
            product.IsConfirmed.Should().BeTrue();
            product.LastUpdated.Should().BeAfter(oldLastUpdated);
        }

        [Fact]
        public void TestSetPrice_WhenValueIsSame_LastUpdatedMustNotChange()
        {
            var price = new PriceBuilder().WithValue(1000).Build();

            var product = new ProductBuilder().WithPrice(price).Build();
            var lastUpdated = product.LastUpdated;

            product.ClearEvents();
            product.SetPrice(price);

            product.DomainEvents.Should().HaveCount(0);
            product.LastUpdated.Should().Be(lastUpdated);
        }
    }
}