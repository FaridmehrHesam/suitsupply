﻿using System;
using FluentAssertions;
using SuitSupply.Framework.Domain.Exceptions;
using SuitSupply.ProductContext.Domain.Properties;
using Xunit;

namespace SuitSupply.ProductContext.Domain.UnitTests.Products.ValueObjects
{
    public class PriceUnitTest
    {
        [Theory]
        [InlineData(10)]
        [InlineData(500)]
        [InlineData(999)]
        public void TestCreate_WhenEverythingIsOk_ValueMustBeSet(double value)
        {
            new PriceBuilder().WithValue(value).Build().Value.Should().Be(value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-500)]
        [InlineData(-999)]
        public void TestCreate_WhenValueIsLessThanOrEqualToZero_MustThrowsException(double value)
        {
            var action = new Action(() => new PriceBuilder().WithValue(value).Build());

            action.Should().Throw<DomainException>().WithMessage(Resources.Product_PriceMustBeHigherThanZero);
        }

        [Theory]
        [InlineData(10)]
        [InlineData(500)]
        [InlineData(999)]
        public void TestNeedsToBeConfirmed_WhenValueIsLessThan999_MustBeFalse(double value)
        {
            new PriceBuilder().WithValue(value).Build().NeedsToBeConfirmed().Should().BeFalse();
        }

        [Theory]
        [InlineData(1000)]
        [InlineData(10000)]
        [InlineData(100000)]
        public void TestNeedsToBeConfirmed_WhenValueIsHigherThan999_MustBeTrue(double value)
        {
            new PriceBuilder().WithValue(value).Build().NeedsToBeConfirmed().Should().BeTrue();
        }

        [Theory]
        [InlineData(10)]
        [InlineData(500)]
        [InlineData(999)]
        public void TestEquality_WhenValuesAreNotSame_MustBeEqual(double value)
        {
            var firstPrice = new PriceBuilder().WithValue(value).Build();
            var secondPrice = new PriceBuilder().WithValue(value).Build();

            firstPrice.Should().Be(secondPrice);
        }

        [Theory]
        [InlineData(10, 500)]
        [InlineData(500, 999)]
        [InlineData(999, 1000)]
        public void TestEquality_WhenValuesAreNotSame_MustNotBeEqual(double firstValue, double secondValue)
        {
            var firstPrice = new PriceBuilder().WithValue(firstValue).Build();
            var secondPrice = new PriceBuilder().WithValue(secondValue).Build();

            firstPrice.Should().NotBe(secondPrice);
        }
    }
}