﻿using SuitSupply.ProductContext.Domain.Products.ValueObjects;

namespace SuitSupply.ProductContext.Domain.UnitTests.Products.ValueObjects
{
    public class PriceBuilder
    {
        private double value;

        public PriceBuilder()
        {
            value = 10;
        }

        public Price Build()
        {
            return new Price(value);
        }

        public PriceBuilder WithValue(double newValue)
        {
            value = newValue;
            return this;
        }
    }
}