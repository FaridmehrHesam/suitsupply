﻿using System;
using FluentAssertions;
using Xunit;

namespace SuitSupply.Framework.Domain.UnitTests
{
    public partial class ValueObjectUnitTest
    {
        [Fact]
        public void TestEquality_WhenValuesAreNotSame_MustNotBeEqual()
        {
            var firstValueObject = new ValueObjectUnitTest {value = Guid.NewGuid()};
            var secondValueObject = new ValueObjectUnitTest {value = Guid.NewGuid()};

            firstValueObject.Should().NotBe(secondValueObject);
        }

        [Fact]
        public void TestEquality_WhenValuesAreSame_MustBeEqual()
        {
            var newValue = Guid.NewGuid();
            var firstValueObject = new ValueObjectUnitTest {value = newValue};
            var secondValueObject = new ValueObjectUnitTest {value = newValue};

            firstValueObject.Should().Be(secondValueObject);
        }
    }
}