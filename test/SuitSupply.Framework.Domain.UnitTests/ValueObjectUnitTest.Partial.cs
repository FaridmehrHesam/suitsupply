﻿using System;
using System.Collections.Generic;

namespace SuitSupply.Framework.Domain.UnitTests
{
    // Using Self Shunt pattern 
    public partial class ValueObjectUnitTest : ValueObject
    {
        private Guid value;

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return value;
        }
    }
}