﻿using SuitSupply.ProductContext.Facade.v1_0.Products.Models;

namespace SuitSupply.ProductContext.RestApi.v1_0.Products
{
    public static class Extensions
    {
        public static void BuildUrl(this ProductReadModel model, string photoBaseUrl)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.Photo)) return;

            model.Photo = photoBaseUrl.Replace("${0}", model.Code);
        }
    }
}