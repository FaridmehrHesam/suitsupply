﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SuitSupply.ProductContext.RestApi.v1_0.Products.Controllers
{
    // For now we just support JPEG
    public partial class ProductsController
    {
        [HttpGet("{productCode}/photo")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetProductPhoto(string productCode)
        {
            var product = await productFacade.GetByCode(productCode).ConfigureAwait(false);
            if (product == null || string.IsNullOrWhiteSpace(product.Photo))
                return NotFound();

            var photoData = await imageStore.Get(product.Photo).ConfigureAwait(false);
            if (photoData == null || photoData.Length == 0)
                return NotFound();

            return File(photoData, "image/jpeg");
        }

        [HttpPost("{productCode}/photo")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UploadProductPhoto(string productCode, [FromForm] IFormFile photo)
        {
            //TODO: What if operation fails in the middle
            try
            {
                if (photo == null)
                    return BadRequest();

                var memoryStream = new MemoryStream();
                await photo.CopyToAsync(memoryStream).ConfigureAwait(false);

                if (memoryStream.Length == 0)
                    return BadRequest();

                var product = await productFacade.GetByCode(productCode).ConfigureAwait(false);
                if (product == null) return NotFound();

                if (!string.IsNullOrWhiteSpace(product.Photo))
                    await imageStore.Delete(product.Photo).ConfigureAwait(false);

                var photoId = await imageStore.Create(memoryStream.ToArray()).ConfigureAwait(false);
                await productFacade.SetPhoto(productCode, photoId).ConfigureAwait(false);

                return Ok();
            }
            catch (IOException)
            {
                return BadRequest();
            }
        }
    }
}