﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SuitSupply.Framework.RestApi;
using SuitSupply.ProductContext.Facade.v1_0.Products;
using SuitSupply.ProductContext.Facade.v1_0.Products.Models;
using SuitSupply.ProductContext.Infrastructure;
using SuitSupply.ProductContext.RestApi.Services;

namespace SuitSupply.ProductContext.RestApi.v1_0.Products.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("rest/api/product/v{version:apiVersion}/[controller]")]
    public partial class ProductsController : ControllerBase
    {
        private readonly ICsvExporter csvExporter;

        private readonly IImageStore imageStore;

        private readonly ProductOptions options;

        private readonly IProductFacade productFacade;

        public ProductsController(
            IProductFacade productFacade,
            ICsvExporter csvExporter,
            IImageStore imageStore,
            IOptions<ProductOptions> options)
        {
            this.productFacade = productFacade;
            this.csvExporter = csvExporter;
            this.imageStore = imageStore;
            this.options = options.Value;
        }

        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(ResponseModel<ProductReadModel>), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateProduct([FromBody] ProductWriteModel model, ApiVersion apiVersion)
        {
            var product = await productFacade.Create(model).ConfigureAwait(false);

            return CreatedAtAction(
                nameof(GetProductByCode),
                new {productCode = product.Code, version = apiVersion.ToString()},
                new ResponseModel<ProductReadModel> {Values = product});
        }

        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseModel<ProductReadModel[]>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllProducts([FromQuery] string search)
        {
            var products = await productFacade.GetAll(search).ConfigureAwait(false);

            foreach (var product in products)
                product.BuildUrl(options.PhotoBaseUrl);

            return Ok(new ResponseModel<ProductReadModel[]> {Values = products});
        }

        [HttpGet("export")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> ExportAllProducts([FromQuery] string search)
        {
            var products = await productFacade.GetAll(search).ConfigureAwait(false);

            foreach (var product in products)
                product.BuildUrl(options.PhotoBaseUrl);

            return File(csvExporter.Export(products), "text/csv", $"Products-{DateTime.Now:yyyy-MM-dd-HH-mm-ss}.csv");
        }

        [HttpGet("{productCode}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ResponseModel<ProductReadModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetProductByCode(string productCode)
        {
            var product = await productFacade.GetByCode(productCode).ConfigureAwait(false);
            if (product == null) return NotFound();

            product.BuildUrl(options.PhotoBaseUrl);

            return Ok(new ResponseModel<ProductReadModel> {Values = product});
        }

        [HttpPut("{productCode}")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> UpdateProduct(string productCode, [FromBody] ProductWriteModel model)
        {
            await productFacade.Update(productCode, model).ConfigureAwait(false);

            return NoContent();
        }

        [HttpPost("{productCode}/confirmPrice")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ConfirmProductPrice(string productCode)
        {
            await productFacade.ConfirmPrice(productCode).ConfigureAwait(false);

            return Ok();
        }

        [HttpDelete("{productCode}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteProduct(string productCode)
        {
            var product = await productFacade.GetByCode(productCode).ConfigureAwait(false);
            if (product == null)
                return NotFound();

            if (!string.IsNullOrWhiteSpace(product.Photo))
                await imageStore.Delete(product.Photo).ConfigureAwait(false);

            await productFacade.Delete(productCode).ConfigureAwait(false);

            return Ok();
        }
    }
}