﻿using System.Collections.Generic;

namespace SuitSupply.ProductContext.RestApi.Services
{
    public interface ICsvExporter
    {
        byte[] Export<T>(IEnumerable<T> data) where T : class;
    }
}