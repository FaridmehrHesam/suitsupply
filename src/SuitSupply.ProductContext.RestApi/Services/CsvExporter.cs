﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using CsvHelper;

namespace SuitSupply.ProductContext.RestApi.Services
{
    public class CsvExporter : ICsvExporter
    {
        public byte[] Export<T>(IEnumerable<T> data) where T : class
        {
            using (var memoryStream = new MemoryStream())
            using (var streamWriter = new StreamWriter(memoryStream))
            using (var csvWriter = new CsvWriter(streamWriter, CultureInfo.InvariantCulture))
            {
                csvWriter.WriteRecords(data);
                streamWriter.Flush();

                return memoryStream.ToArray();
            }
        }
    }
}