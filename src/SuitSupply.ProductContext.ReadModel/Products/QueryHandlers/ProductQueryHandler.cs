﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SuitSupply.Framework.ReadModel;
using SuitSupply.ProductContext.ReadModel.Contract.Products;
using SuitSupply.ProductContext.ReadModel.Contract.Products.Queries;

namespace SuitSupply.ProductContext.ReadModel.Products.QueryHandlers
{
    public class ProductQueryHandler : IQueryHandler<ProductQuery, Product>
    {
        private readonly ProductReadDbContext dbContext;

        public ProductQueryHandler(ProductReadDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public Task<Product> Handle(ProductQuery query, CancellationToken cancellationToken = default)
        {
            return dbContext.Products
                .AsNoTracking()
                .SingleOrDefaultAsync(i => i.Code == query.Code, cancellationToken);
        }
    }
}