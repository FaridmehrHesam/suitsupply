﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SuitSupply.Framework.ReadModel;
using SuitSupply.ProductContext.ReadModel.Contract.Products;
using SuitSupply.ProductContext.ReadModel.Contract.Products.Queries;

namespace SuitSupply.ProductContext.ReadModel.Products.QueryHandlers
{
    public class ProductCollectionQueryHandler : IQueryHandler<ProductCollectionQuery, Product[]>
    {
        private readonly ProductReadDbContext dbContext;

        public ProductCollectionQueryHandler(ProductReadDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public Task<Product[]> Handle(ProductCollectionQuery query,
            CancellationToken cancellationToken = new CancellationToken())
        {
            var products = dbContext.Products.OrderBy(i => i.Code).AsNoTracking();
            var search = query.Search;

            if (!string.IsNullOrWhiteSpace(search))
                products = products.Where(i => i.Code.Contains(search) || i.Name.Contains(search));

            return products.ToArrayAsync(cancellationToken);
        }
    }
}