﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SuitSupply.ProductContext.ReadModel.Contract.Products;

namespace SuitSupply.ProductContext.ReadModel
{
    public class ProductReadDbContext : DbContext
    {
        public ProductReadDbContext(DbContextOptions<ProductReadDbContext> options)
            : base(options)
        {
        }


        public DbSet<Product> Products { get; protected set; }


        public override int SaveChanges()
        {
            throw new InvalidOperationException();
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            throw new InvalidOperationException();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            throw new InvalidOperationException();
        }

        public override Task<int> SaveChangesAsync(
            bool acceptAllChangesOnSuccess,
            CancellationToken cancellationToken = default)
        {
            throw new InvalidOperationException();
        }
    }
}