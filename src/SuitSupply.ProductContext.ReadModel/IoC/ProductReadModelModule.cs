﻿using Autofac;
using SuitSupply.Framework.ReadModel;

namespace SuitSupply.ProductContext.ReadModel.IoC
{
    public class ProductReadModelModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(IQueryHandler<,>))
                .InstancePerLifetimeScope();

            base.Load(builder);
        }
    }
}