﻿using System.Threading;
using System.Threading.Tasks;

namespace SuitSupply.Framework.ReadModel
{
    public interface IQueryBus
    {
        Task<TResponse> Get<TQuery, TResponse>(TQuery query, CancellationToken cancellationToken = default)
            where TQuery : IQuery<TResponse>;
    }
}