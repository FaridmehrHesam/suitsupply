﻿using System.Threading;
using System.Threading.Tasks;

namespace SuitSupply.Framework.ReadModel
{
    public interface IQueryHandler<in TQuery, TResponse> where TQuery : IQuery<TResponse>
    {
        Task<TResponse> Handle(TQuery query, CancellationToken cancellationToken = default);
    }
}