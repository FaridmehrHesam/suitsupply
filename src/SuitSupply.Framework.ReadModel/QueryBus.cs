﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace SuitSupply.Framework.ReadModel
{
    public sealed class QueryBus : IQueryBus
    {
        private readonly IServiceProvider serviceProvider;

        public QueryBus(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public Task<TResponse> Get<TQuery, TResponse>(TQuery query, CancellationToken cancellationToken = default)
            where TQuery : IQuery<TResponse>
        {
            return serviceProvider.GetService<IQueryHandler<TQuery, TResponse>>().Handle(query, cancellationToken);
        }
    }
}