﻿namespace SuitSupply.ProductContext.Infrastructure
{
    public class LocalImageStoreOptions
    {
        public string DirectoryPath { get; set; }
    }
}