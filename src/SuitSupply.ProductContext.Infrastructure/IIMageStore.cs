﻿using System.Threading;
using System.Threading.Tasks;

namespace SuitSupply.ProductContext.Infrastructure
{
    public interface IImageStore
    {
        Task<string> Create(byte[] data, CancellationToken cancellationToken = default);

        Task<byte[]> Get(string imageId, CancellationToken cancellationToken = default);

        Task Delete(string imageId, CancellationToken cancellationToken = default);
    }
}