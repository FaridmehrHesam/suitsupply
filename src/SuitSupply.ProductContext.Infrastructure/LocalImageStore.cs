﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace SuitSupply.ProductContext.Infrastructure
{
    public class LocalImageStore : IImageStore
    {
        private readonly List<byte[]> jpegSignatures = new List<byte[]>
        {
            new byte[] {0xFF, 0xD8, 0xFF, 0xE0},
            new byte[] {0xFF, 0xD8, 0xFF, 0xE2},
            new byte[] {0xFF, 0xD8, 0xFF, 0xE3}
        };

        private readonly LocalImageStoreOptions options;

        public LocalImageStore(IOptions<LocalImageStoreOptions> options)
        {
            this.options = options.Value;
        }

        public async Task<string> Create(byte[] data, CancellationToken cancellationToken = default)
        {
            if (!IsJpeg(data))
                throw new InvalidOperationException();

            if (!Directory.Exists(options.DirectoryPath))
                Directory.CreateDirectory(options.DirectoryPath);

            var fileName = Guid.NewGuid().ToString();
            var filePath = Path.Combine(options.DirectoryPath, fileName);

            await File.WriteAllBytesAsync(filePath, data, cancellationToken).ConfigureAwait(false);

            return fileName;
        }

        public async Task<byte[]> Get(string imageId, CancellationToken cancellationToken = default)
        {
            var filePath = Path.Combine(options.DirectoryPath, imageId);

            return !File.Exists(filePath)
                ? Array.Empty<byte>()
                : await File.ReadAllBytesAsync(filePath, cancellationToken).ConfigureAwait(false);
        }

        public Task Delete(string imageId, CancellationToken cancellationToken = default)
        {
            var filePath = Path.Combine(options.DirectoryPath, imageId);
            if (File.Exists(filePath)) File.Delete(filePath);

            return Task.CompletedTask;
        }

        private bool IsJpeg(byte[] data)
        {
            return jpegSignatures.Any(i => data.Take(i.Length).SequenceEqual(i));
        }
    }
}