import Vue from 'vue';
import Antd from 'ant-design-vue';
import moment from 'moment';
import 'ant-design-vue/dist/antd.css';

import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';

Vue.config.productionTip = false;
Vue.prototype.moment = moment;

Vue.use(Antd);

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
