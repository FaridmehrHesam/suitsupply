import axios from 'axios';
import Product from '@/models/product';

export default class Products {
    baseUrl: string = 'http://localhost:5000/rest/api/product/v1.0/products';

    async add(product: Product): Promise<Product> {
        try {
            let response = await axios.post(this.baseUrl, {
                code: product.code,
                name: product.name,
                price: product.price
            });
            return response.data.values as Product;
        } catch (exception) {
            if (!exception.response) throw 'Unable to connect to server.';
            throw exception.response.data.errorMessage;
        }
    }

    async getAll(search: string): Promise<Product[]> {
        try {
            let response = await axios.get(`${this.baseUrl}?search=${search}`);
            let products = response.data.values as Product[];

            products.forEach(item => (item.id = item.code));

            return products;
        } catch (exception) {
            if (!exception.response) throw 'Unable to connect to server.';
            throw exception.response.data.errorMessage;
        }
    }

    async edit(id: string, product: Product) {
        try {
            await axios.put(`${this.baseUrl}/${id}`, {
                code: product.code,
                name: product.name,
                price: product.price
            });
        } catch (exception) {
            if (!exception.response) throw 'Unable to connect to server.';
            throw exception.response.data.errorMessage;
        }
    }

    async confirmPrice(id: string) {
        try {
            await axios.post(`${this.baseUrl}/${id}/confirmPrice`);
        } catch (exception) {
            if (!exception.response) throw 'Unable to connect to server.';
            throw exception.response.data.errorMessage;
        }
    }

    async remove(id: string) {
        try {
            await axios.delete(`${this.baseUrl}/${id}`);
        } catch (exception) {
            if (!exception.response) throw 'Unable to connect to server.';
            throw exception.response.data.errorMessage;
        }
    }
}
