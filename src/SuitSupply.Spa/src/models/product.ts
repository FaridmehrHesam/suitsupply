export default interface Product {
    id: string;
    code: string;
    name: string;
    photo: string;
    price: number;
    isConfirmed: boolean;
    lastUpdated: string;
}
