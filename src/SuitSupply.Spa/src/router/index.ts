import Vue from 'vue';
import VueRouter from 'vue-router';
import ProductTable from '../components/products/table.vue';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'products',
        component: ProductTable
    }
];

const router = new VueRouter({
    routes
});

export default router;
