﻿using SuitSupply.Framework.ReadModel;

namespace SuitSupply.ProductContext.ReadModel.Contract.Products.Queries
{
    public class ProductCollectionQuery : IQuery<Product[]>
    {
        public string Search { get; set; }

        //TODO: Paging must be implemented
    }
}