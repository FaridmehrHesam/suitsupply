﻿using SuitSupply.Framework.ReadModel;

namespace SuitSupply.ProductContext.ReadModel.Contract.Products.Queries
{
    public class ProductQuery : IQuery<Product>
    {
        public string Code { get; set; }
    }
}