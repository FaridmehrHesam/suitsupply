﻿using System;

namespace SuitSupply.ProductContext.ReadModel.Contract.Products
{
    public class Product
    {
        public Guid Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Photo { get; set; }

        public double Price { get; set; }

        public bool IsConfirmed { get; set; }

        public DateTime LastUpdated { get; set; }
    }
}