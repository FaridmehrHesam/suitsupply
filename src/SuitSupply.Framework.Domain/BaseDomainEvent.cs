﻿using System;
using System.Collections.Generic;

namespace SuitSupply.Framework.Domain
{
    public class BaseDomainEvent : IDomainEvent
    {
        protected BaseDomainEvent(Guid aggregateId)
        {
            AggregateId = aggregateId;
            EventTime = DateTime.UtcNow;
        }

        public Guid AggregateId { get; }

        public DateTime EventTime { get; }

        public virtual Dictionary<string, string> Flatten()
        {
            var eventData = new Dictionary<string, string>();

            foreach (var property in GetType().GetProperties())
            {
                if (property.Name == nameof(AggregateId) || property.Name == nameof(EventTime)) continue;

                eventData.Add(property.Name, property.GetValue(this)?.ToString());
            }

            return eventData;
        }
    }
}