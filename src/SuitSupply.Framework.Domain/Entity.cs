﻿using System;
using System.Collections.Generic;
using SuitSupply.Framework.Domain.Exceptions;

namespace SuitSupply.Framework.Domain
{
    public abstract class Entity
    {
        private readonly List<IDomainEvent> domainEvents;

        protected Entity()
        {
            Id = Guid.NewGuid();
            domainEvents = new List<IDomainEvent>();
        }

        public Guid Id { get; protected set; }

        public IReadOnlyCollection<IDomainEvent> DomainEvents => domainEvents.AsReadOnly();

        protected void AddEvent(IDomainEvent domainEvent)
        {
            domainEvents.Add(domainEvent);
        }

        public void ClearEvents()
        {
            domainEvents.Clear();
        }

        protected void CheckRule(IBusinessRule rule)
        {
            if (!rule.IsBroken()) return;

            throw new DomainException(rule.Message);
        }
    }
}