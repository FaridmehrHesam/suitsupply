﻿using System;
using System.Collections.Generic;

namespace SuitSupply.Framework.Domain
{
    public interface IDomainEvent
    {
        DateTime EventTime { get; }

        Guid AggregateId { get; }

        Dictionary<string, string> Flatten();
    }
}