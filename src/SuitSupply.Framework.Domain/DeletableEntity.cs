﻿namespace SuitSupply.Framework.Domain
{
    public class DeletableEntity : Entity
    {
        private bool isDeleted;

        public bool IsDeleted()
        {
            return isDeleted;
        }

        protected void MarkAsDeleted()
        {
            isDeleted = true;
        }
    }
}