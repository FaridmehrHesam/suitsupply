﻿using System;

namespace SuitSupply.Framework.Domain
{
    public abstract class TrackableEntity : DeletableEntity
    {
        public DateTime LastUpdated { get; private set; }

        protected void TrackUpdate()
        {
            LastUpdated = DateTime.UtcNow;
        }
    }
}