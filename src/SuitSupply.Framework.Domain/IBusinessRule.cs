﻿namespace SuitSupply.Framework.Domain
{
    public interface IBusinessRule
    {
        string Message { get; }

        bool IsBroken();
    }
}