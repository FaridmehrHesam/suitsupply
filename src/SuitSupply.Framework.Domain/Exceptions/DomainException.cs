﻿using System;
using System.Runtime.Serialization;

namespace SuitSupply.Framework.Domain.Exceptions
{
    [Serializable]
    public class DomainException : ApplicationException
    {
        public DomainException(string message)
            : base(message)
        {
        }

        public DomainException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected DomainException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}