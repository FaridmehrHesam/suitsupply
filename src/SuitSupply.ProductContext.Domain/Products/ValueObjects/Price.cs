﻿using System.Collections.Generic;
using SuitSupply.Framework.Domain;
using SuitSupply.ProductContext.Domain.Products.Rules;

namespace SuitSupply.ProductContext.Domain.Products.ValueObjects
{
    public class Price : ValueObject
    {
        private Price()
        {
            // For EF
        }

        public Price(double value)
        {
            CheckRule(new ProductPriceMustBeHigherThanZeroRule(value));

            Value = value;
        }

        public double Value { get; private set; }

        public bool NeedsToBeConfirmed()
        {
            // TODO: Maybe it is better to get confirmation value from outside as a parameter or as domain constant
            return Value > 999;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Value;
        }
    }
}