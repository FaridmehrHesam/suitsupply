﻿using System;
using SuitSupply.Framework.Domain;

namespace SuitSupply.ProductContext.Domain.Products.Events
{
    public class ProductConfirmedEvent : BaseDomainEvent
    {
        public ProductConfirmedEvent(Guid aggregateId) : base(aggregateId)
        {
        }
    }
}