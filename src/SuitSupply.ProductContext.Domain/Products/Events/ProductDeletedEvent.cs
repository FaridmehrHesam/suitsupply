﻿using System;
using SuitSupply.Framework.Domain;

namespace SuitSupply.ProductContext.Domain.Products.Events
{
    public class ProductDeletedEvent : BaseDomainEvent
    {
        public ProductDeletedEvent(Guid aggregateId) : base(aggregateId)
        {
        }
    }
}