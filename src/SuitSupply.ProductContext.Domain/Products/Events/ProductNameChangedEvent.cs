﻿using System;
using SuitSupply.Framework.Domain;

namespace SuitSupply.ProductContext.Domain.Products.Events
{
    public class ProductNameChangedEvent : BaseDomainEvent
    {
        public ProductNameChangedEvent(Guid aggregateId, string oldValue, string newValue) : base(aggregateId)
        {
            OldValue = oldValue;
            NewValue = newValue;
        }

        public string OldValue { get; }

        public string NewValue { get; }
    }
}