﻿using System;
using SuitSupply.Framework.Domain;

namespace SuitSupply.ProductContext.Domain.Products.Events
{
    public class ProductCreatedEvent : BaseDomainEvent
    {
        public ProductCreatedEvent(Guid aggregateId) : base(aggregateId)
        {
        }
    }
}