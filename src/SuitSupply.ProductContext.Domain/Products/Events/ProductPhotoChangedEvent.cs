﻿using System;
using SuitSupply.Framework.Domain;

namespace SuitSupply.ProductContext.Domain.Products.Events
{
    public class ProductPhotoChangedEvent : BaseDomainEvent
    {
        public ProductPhotoChangedEvent(Guid aggregateId) : base(aggregateId)
        {
        }
    }
}