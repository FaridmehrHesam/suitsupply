﻿using System;
using SuitSupply.Framework.Domain;

namespace SuitSupply.ProductContext.Domain.Products.Events
{
    public class ProductPriceChangedEvent : BaseDomainEvent
    {
        public ProductPriceChangedEvent(Guid aggregateId, double? oldValue, double newValue) : base(aggregateId)
        {
            OldValue = oldValue;
            NewValue = newValue;
        }

        public double? OldValue { get; }

        public double NewValue { get; }
    }
}