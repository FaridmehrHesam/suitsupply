﻿using System.Threading;
using System.Threading.Tasks;

namespace SuitSupply.ProductContext.Domain.Products.Services
{
    public interface IProductRepository
    {
        void Add(Product product);

        Task<Product> GetByCode(string code, CancellationToken cancellationToken = default);

        void Remove(Product product);
    }
}