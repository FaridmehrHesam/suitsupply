﻿using SuitSupply.Framework.Domain;
using SuitSupply.ProductContext.Domain.Properties;

namespace SuitSupply.ProductContext.Domain.Products.Rules
{
    public class ProductPriceMustBeHigherThanZeroRule : IBusinessRule
    {
        private readonly double value;

        public ProductPriceMustBeHigherThanZeroRule(double value)
        {
            this.value = value;
        }

        public string Message { get; } = Resources.Product_PriceMustBeHigherThanZero;

        public bool IsBroken()
        {
            return value <= 0;
        }
    }
}