﻿using SuitSupply.Framework.Domain;
using SuitSupply.ProductContext.Domain.Properties;

namespace SuitSupply.ProductContext.Domain.Products.Rules
{
    public class ProductPriceCanBeConfirmedOnceRule : IBusinessRule
    {
        private readonly bool isConfirmed;

        public ProductPriceCanBeConfirmedOnceRule(bool isConfirmed)
        {
            this.isConfirmed = isConfirmed;
        }

        public string Message { get; } = Resources.Product_PriceCanBeConfirmedOnce;

        public bool IsBroken()
        {
            return isConfirmed;
        }
    }
}