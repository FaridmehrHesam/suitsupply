﻿using SuitSupply.Framework.Domain;
using SuitSupply.ProductContext.Domain.Properties;

namespace SuitSupply.ProductContext.Domain.Products.Rules
{
    public class ProductNameCanNotBeEmptyRule : IBusinessRule
    {
        private readonly string name;

        public ProductNameCanNotBeEmptyRule(string name)
        {
            this.name = name;
        }

        public string Message { get; } = Resources.Product_NameCanNotBeEmpty;

        public bool IsBroken()
        {
            return string.IsNullOrWhiteSpace(name);
        }
    }
}