﻿using SuitSupply.Framework.Domain;
using SuitSupply.ProductContext.Domain.Properties;

namespace SuitSupply.ProductContext.Domain.Products.Rules
{
    public class ProductCodeCanNotBeEmptyRule : IBusinessRule
    {
        private readonly string code;

        public ProductCodeCanNotBeEmptyRule(string code)
        {
            this.code = code;
        }

        public string Message { get; } = Resources.Product_CodeCanNotBeEmpty;

        public bool IsBroken()
        {
            return string.IsNullOrWhiteSpace(code);
        }
    }
}