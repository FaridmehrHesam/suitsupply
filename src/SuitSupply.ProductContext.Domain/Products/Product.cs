﻿using System;
using SuitSupply.Framework.Domain;
using SuitSupply.ProductContext.Domain.Products.Events;
using SuitSupply.ProductContext.Domain.Products.Rules;
using SuitSupply.ProductContext.Domain.Products.ValueObjects;

namespace SuitSupply.ProductContext.Domain.Products
{
    public class Product : TrackableEntity, IAggregateRoot
    {
        private Product()
        {
            //For EF
        }

        public string Code { get; private set; }

        public string Name { get; private set; }

        public Price Price { get; private set; }

        public string Photo { get; private set; }

        public bool IsConfirmed { get; private set; }

        public static Product Create(string code, string name, Price price)
        {
            var product = new Product();

            product.AddEvent(new ProductCreatedEvent(product.Id));

            product.SetCode(code);
            product.SetName(name);
            product.SetPrice(price);

            return product;
        }

        public void SetCode(string code)
        {
            CheckRule(new ProductCodeCanNotBeEmptyRule(code));

            if (Code == code)
                return;

            AddEvent(new ProductCodeChangedEvent(Id, Code, code));
            TrackUpdate();

            Code = code;
        }

        public void SetName(string name)
        {
            CheckRule(new ProductNameCanNotBeEmptyRule(name));

            if (Name == name)
                return;

            AddEvent(new ProductNameChangedEvent(Id, Name, name));
            TrackUpdate();

            Name = name;
        }

        public void SetPhoto(string photo)
        {
            if (Photo == photo)
                return;

            AddEvent(new ProductPhotoChangedEvent(Id));
            TrackUpdate();

            Photo = photo;
        }

        public void SetPrice(Price price)
        {
            if (Price != null && Price.Equals(price))
                return;

            AddEvent(new ProductPriceChangedEvent(Id, Price?.Value, price.Value));
            TrackUpdate();

            Price = price;
            IsConfirmed = !price.NeedsToBeConfirmed();
        }

        public void ConfirmPrice()
        {
            CheckRule(new ProductPriceCanBeConfirmedOnceRule(IsConfirmed));

            AddEvent(new ProductConfirmedEvent(Id));
            TrackUpdate();

            IsConfirmed = true;
        }

        public void Delete()
        {
            if (IsDeleted())
                throw new InvalidOperationException();

            AddEvent(new ProductDeletedEvent(Id));
            MarkAsDeleted();
        }
    }
}