﻿using System;

namespace SuitSupply.ProductContext.Facade.v1_0.Products.Models
{
    public class ProductReadModel
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public string Photo { get; set; }

        public double Price { get; set; }

        public bool IsConfirmed { get; set; }

        public DateTime LastUpdated { get; set; }
    }
}