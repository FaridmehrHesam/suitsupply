﻿namespace SuitSupply.ProductContext.Facade.v1_0.Products.Models
{
    public class ProductWriteModel
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }
    }
}