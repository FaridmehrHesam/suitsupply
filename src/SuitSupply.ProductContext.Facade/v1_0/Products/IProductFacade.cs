﻿using System.Threading;
using System.Threading.Tasks;
using SuitSupply.ProductContext.Facade.v1_0.Products.Models;

namespace SuitSupply.ProductContext.Facade.v1_0.Products
{
    public interface IProductFacade
    {
        Task<ProductReadModel> Create(ProductWriteModel model, CancellationToken cancellationToken = default);

        Task<ProductReadModel[]> GetAll(string search, CancellationToken cancellationToken = default);

        Task<ProductReadModel> GetByCode(string code, CancellationToken cancellationToken = default);

        Task Update(string code, ProductWriteModel model, CancellationToken cancellationToken = default);

        Task ConfirmPrice(string code, CancellationToken cancellationToken = default);

        Task SetPhoto(string code, string photo, CancellationToken cancellationToken = default);

        Task Delete(string code, CancellationToken cancellationToken = default);
    }
}