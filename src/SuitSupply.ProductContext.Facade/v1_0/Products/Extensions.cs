﻿using SuitSupply.ProductContext.Application.Contract.Products.Commands;
using SuitSupply.ProductContext.Facade.v1_0.Products.Models;
using SuitSupply.ProductContext.ReadModel.Contract.Products;

namespace SuitSupply.ProductContext.Facade.v1_0.Products
{
    internal static class Extensions
    {
        internal static CreateProductCommand ToCreateCommand(this ProductWriteModel model)
        {
            return new CreateProductCommand
            {
                Code = model.Code,
                Name = model.Name,
                Price = model.Price
            };
        }

        internal static UpdateProductCommand ToUpdateCommand(this ProductWriteModel model, string currentCode)
        {
            return new UpdateProductCommand
            {
                CurrentCode = currentCode,
                Code = model.Code,
                Name = model.Name,
                Price = model.Price
            };
        }

        internal static ProductReadModel ToReadModel(this Product product)
        {
            return new ProductReadModel
            {
                Code = product.Code,
                Name = product.Name,
                Photo = product.Photo,
                Price = product.Price,
                IsConfirmed = product.IsConfirmed,
                LastUpdated = product.LastUpdated
            };
        }
    }
}