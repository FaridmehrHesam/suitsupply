﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SuitSupply.Framework.Application;
using SuitSupply.Framework.ReadModel;
using SuitSupply.ProductContext.Application.Contract.Products.Commands;
using SuitSupply.ProductContext.Facade.v1_0.Products.Models;
using SuitSupply.ProductContext.ReadModel.Contract.Products;
using SuitSupply.ProductContext.ReadModel.Contract.Products.Queries;

namespace SuitSupply.ProductContext.Facade.v1_0.Products
{
    public class ProductFacade : IProductFacade
    {
        private readonly ICommandBus commandBus;

        private readonly IQueryBus queryBus;

        public ProductFacade(ICommandBus commandBus, IQueryBus queryBus)
        {
            this.commandBus = commandBus;
            this.queryBus = queryBus;
        }

        public async Task<ProductReadModel> Create(
            ProductWriteModel model,
            CancellationToken cancellationToken = default)
        {
            await commandBus.Dispatch(model.ToCreateCommand(), cancellationToken).ConfigureAwait(false);

            return await GetByCode(model.Code, cancellationToken).ConfigureAwait(false);
        }

        public async Task<ProductReadModel[]> GetAll(string search, CancellationToken cancellationToken = default)
        {
            var query = new ProductCollectionQuery {Search = search};
            var products = await queryBus
                .Get<ProductCollectionQuery, Product[]>(query, cancellationToken)
                .ConfigureAwait(false);

            return products.Select(i => i.ToReadModel()).ToArray();
        }

        public async Task<ProductReadModel> GetByCode(string code, CancellationToken cancellationToken = default)
        {
            var query = new ProductQuery {Code = code};
            var product = await queryBus.Get<ProductQuery, Product>(query, cancellationToken).ConfigureAwait(false);

            return product?.ToReadModel();
        }

        public Task Update(string code, ProductWriteModel model, CancellationToken cancellationToken = default)
        {
            var command = model.ToUpdateCommand(code);

            return commandBus.Dispatch(command, cancellationToken);
        }

        public Task ConfirmPrice(string code, CancellationToken cancellationToken = default)
        {
            var command = new ConfirmProductPriceCommand {Code = code};

            return commandBus.Dispatch(command, cancellationToken);
        }

        public Task SetPhoto(string code, string photo, CancellationToken cancellationToken = default)
        {
            var command = new SetProductPhotoCommand {Code = code, Photo = photo};

            return commandBus.Dispatch(command, cancellationToken);
        }

        public Task Delete(string code, CancellationToken cancellationToken = default)
        {
            var command = new DeleteProductCommand {Code = code};

            return commandBus.Dispatch(command, cancellationToken);
        }
    }
}