﻿using SuitSupply.Framework.Application;

namespace SuitSupply.ProductContext.Application.Contract.Products.Commands
{
    public class DeleteProductCommand : ICommand
    {
        public string Code { get; set; }
    }
}