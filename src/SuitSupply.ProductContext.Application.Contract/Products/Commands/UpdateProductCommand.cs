﻿using SuitSupply.Framework.Application;

namespace SuitSupply.ProductContext.Application.Contract.Products.Commands
{
    public class UpdateProductCommand : ICommand
    {
        public string CurrentCode { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }
    }
}