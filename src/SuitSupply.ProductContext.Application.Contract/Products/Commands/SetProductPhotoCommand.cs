﻿using SuitSupply.Framework.Application;

namespace SuitSupply.ProductContext.Application.Contract.Products.Commands
{
    public class SetProductPhotoCommand : ICommand
    {
        public string Code { get; set; }

        public string Photo { get; set; }
    }
}