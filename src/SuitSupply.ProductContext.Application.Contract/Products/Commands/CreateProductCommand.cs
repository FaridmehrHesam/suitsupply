﻿using SuitSupply.Framework.Application;

namespace SuitSupply.ProductContext.Application.Contract.Products.Commands
{
    public class CreateProductCommand : ICommand
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }
    }
}