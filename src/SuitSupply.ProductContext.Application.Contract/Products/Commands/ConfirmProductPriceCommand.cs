﻿using SuitSupply.Framework.Application;

namespace SuitSupply.ProductContext.Application.Contract.Products.Commands
{
    public class ConfirmProductPriceCommand : ICommand
    {
        public string Code { get; set; }
    }
}