﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using SuitSupply.ProductContext.Domain.Products;
using SuitSupply.ProductContext.Persistence.Audits;
using SuitSupply.ProductContext.Persistence.Products.EntityConfigurations;

namespace SuitSupply.ProductContext.Persistence
{
    public class ProductWriteDbContext : DbContext
    {
        public ProductWriteDbContext(DbContextOptions<ProductWriteDbContext> options)
            : base(options)
        {
        }

        public DbSet<Audit> Audits { get; protected set; }

        public DbSet<Product> Products { get; protected set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProductEntityConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }

#if DEBUG
    public class DesignTimeResourceDbContextFactory : IDesignTimeDbContextFactory<ProductWriteDbContext>
    {
        public ProductWriteDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ProductWriteDbContext>();

            builder.UseSqlServer("Server=192.168.200.67;Database=ProductContext;User Id=sa;Password=Abcd123;");

            return new ProductWriteDbContext(builder.Options);
        }
    }
#endif
}