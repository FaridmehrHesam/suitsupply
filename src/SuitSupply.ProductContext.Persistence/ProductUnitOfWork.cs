﻿using System.Threading;
using System.Threading.Tasks;
using SuitSupply.Framework.Application;
using SuitSupply.Framework.Infrastructure;

namespace SuitSupply.ProductContext.Persistence
{
    public class ProductUnitOfWork : IUnitOfWork
    {
        private readonly ProductWriteDbContext dbContext;

        private readonly IEventBus eventBus;

        public ProductUnitOfWork(ProductWriteDbContext dbContext, IEventBus eventBus)
        {
            this.dbContext = dbContext;
            this.eventBus = eventBus;
        }

        public Task BeginTransaction(CancellationToken cancellationToken = default)
        {
            return dbContext.Database.BeginTransactionAsync(cancellationToken);
        }

        public async Task CommitTransaction(CancellationToken cancellationToken = default)
        {
            dbContext.CheckDeletableEntities();
            dbContext.AuditDomainEvents();
            await dbContext.DispatchDomainEvents(eventBus, cancellationToken).ConfigureAwait(false);
            dbContext.ClearDomainEvents();

            await dbContext.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

            dbContext.Database.CommitTransaction();
        }

        public Task RollbackTransaction(CancellationToken cancellationToken = default)
        {
            dbContext.Database.RollbackTransaction();
            return Task.CompletedTask;
        }
    }
}