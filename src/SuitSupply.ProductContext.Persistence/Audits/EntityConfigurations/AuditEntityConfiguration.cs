﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SuitSupply.ProductContext.Persistence.Audits.EntityConfigurations
{
    public class AuditEntityConfiguration : IEntityTypeConfiguration<Audit>
    {
        public void Configure(EntityTypeBuilder<Audit> builder)
        {
            builder.Property(i => i.Id).ValueGeneratedNever();
            builder.Property(i => i.AggregateId).IsRequired();
            builder.Property(i => i.EventName).IsRequired();
            builder.Property(i => i.Data).IsRequired();
        }
    }
}