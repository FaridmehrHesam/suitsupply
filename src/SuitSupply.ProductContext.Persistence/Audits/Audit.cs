﻿using System;

namespace SuitSupply.ProductContext.Persistence.Audits
{
    public class Audit
    {
        public Guid Id { get; set; }

        public Guid AggregateId { get; set; }

        public string EventName { get; set; }

        public string Data { get; set; }

        public DateTime EventTime { get; set; }
    }
}