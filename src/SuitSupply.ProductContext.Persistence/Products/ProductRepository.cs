﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SuitSupply.ProductContext.Domain.Products;
using SuitSupply.ProductContext.Domain.Products.Services;

namespace SuitSupply.ProductContext.Persistence.Products
{
    public class ProductRepository : IProductRepository
    {
        private readonly DbSet<Product> products;

        public ProductRepository(ProductWriteDbContext dbContext)
        {
            products = dbContext.Products;
        }

        public void Add(Product product)
        {
            products.Add(product);
        }

        public Task<Product> GetByCode(string code, CancellationToken cancellationToken = default)
        {
            return products.SingleOrDefaultAsync(i => i.Code == code, cancellationToken);
        }

        public void Remove(Product product)
        {
            product.Delete();
            products.Remove(product);
        }
    }
}