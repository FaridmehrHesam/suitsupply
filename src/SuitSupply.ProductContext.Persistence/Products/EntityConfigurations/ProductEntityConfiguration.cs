﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SuitSupply.ProductContext.Domain.Products;

namespace SuitSupply.ProductContext.Persistence.Products.EntityConfigurations
{
    public class ProductEntityConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.Property(i => i.Id).ValueGeneratedNever();
            builder.Property(i => i.Code).IsRequired();
            builder.Property(i => i.Name).IsRequired();

            builder.HasIndex(i => i.Code).IsUnique();

            builder.OwnsOne(i => i.Price, context => context
                .Property(i => i.Value)
                .HasColumnName("Price"));
        }
    }
}