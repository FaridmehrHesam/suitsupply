﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SuitSupply.Framework.Domain;
using SuitSupply.Framework.Infrastructure;
using SuitSupply.ProductContext.Persistence.Audits;

namespace SuitSupply.ProductContext.Persistence
{
    public static class Extensions
    {
        public static void CheckDeletableEntities(this ProductWriteDbContext dbContext)
        {
            if (dbContext.ChangeTracker.Entries<DeletableEntity>()
                .Any(i => i.State == EntityState.Deleted && !i.Entity.IsDeleted()))
                throw new InvalidOperationException();
        }

        public static void AuditDomainEvents(this ProductWriteDbContext dbContext)
        {
            var entityEntries = dbContext.ChangeTracker.Entries<Entity>()
                .Where(i => i.Entity.DomainEvents.Any())
                .ToArray();

            //TODO: User data must be added
            foreach (var entry in entityEntries)
            {
                foreach (var domainEvent in entry.Entity.DomainEvents)
                    dbContext.Audits.Add(
                        new Audit
                        {
                            Id = Guid.NewGuid(),
                            AggregateId = domainEvent.AggregateId,
                            EventName = domainEvent.GetType().Name,
                            EventTime = domainEvent.EventTime,
                            Data = JsonConvert.SerializeObject(domainEvent.Flatten())
                        });
            }
        }

        public static async Task DispatchDomainEvents(
            this ProductWriteDbContext dbContext,
            IEventBus eventBus,
            CancellationToken cancellationToken)
        {
            var entityEntries = dbContext.ChangeTracker.Entries<Entity>()
                .Where(i => i.Entity.DomainEvents.Any())
                .ToArray();

            foreach (var entry in entityEntries)
            {
                foreach (var domainEvent in entry.Entity.DomainEvents)
                    await eventBus.Dispatch(domainEvent, cancellationToken).ConfigureAwait(false);
            }
        }

        public static void ClearDomainEvents(this ProductWriteDbContext dbContext)
        {
            var entityEntries = dbContext.ChangeTracker.Entries<Entity>()
                .Where(i => i.Entity.DomainEvents.Any())
                .ToArray();

            foreach (var entry in entityEntries)
                entry.Entity.ClearEvents();
        }
    }
}