﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace SuitSupply.Framework.Application
{
    public sealed class CommandBus : ICommandBus
    {
        private readonly IServiceProvider serviceProvider;

        public CommandBus(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public Task Dispatch<T>(T command, CancellationToken cancellationToken = default)
            where T : ICommand
        {
            return serviceProvider.GetService<ICommandHandler<T>>().Handle(command, cancellationToken);
        }
    }
}