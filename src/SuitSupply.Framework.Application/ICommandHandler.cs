﻿using System.Threading;
using System.Threading.Tasks;

namespace SuitSupply.Framework.Application
{
    public interface ICommandHandler<in T> where T : ICommand
    {
        Task Handle(T command, CancellationToken cancellationToken = default);
    }
}