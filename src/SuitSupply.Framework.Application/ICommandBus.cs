﻿using System.Threading;
using System.Threading.Tasks;

namespace SuitSupply.Framework.Application
{
    public interface ICommandBus
    {
        Task Dispatch<T>(T command, CancellationToken cancellationToken = default) where T : ICommand;
    }
}