﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace SuitSupply.Framework.Application.Decorators
{
    public class CommandHandlerLoggingDecorator<T> : ICommandHandler<T>
        where T : ICommand
    {
        private readonly ICommandHandler<T> commandHandler;

        private readonly ILogger<CommandHandlerLoggingDecorator<T>> logger;

        public CommandHandlerLoggingDecorator(
            ILogger<CommandHandlerLoggingDecorator<T>> logger,
            ICommandHandler<T> commandHandler)
        {
            this.logger = logger;
            this.commandHandler = commandHandler;
        }

        public async Task Handle(T command, CancellationToken cancellationToken = default)
        {
            var commandName = command.GetType().Name;

            try
            {
                logger.LogInformation($"Handling command {commandName} started.");
                await commandHandler.Handle(command, cancellationToken).ConfigureAwait(false);
                logger.LogInformation($"Handling command {commandName} finished.");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Handling command {commandName} failed.");
                throw;
            }
        }
    }
}