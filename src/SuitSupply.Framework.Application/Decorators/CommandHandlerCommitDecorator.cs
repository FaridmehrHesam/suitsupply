﻿using System.Threading;
using System.Threading.Tasks;

namespace SuitSupply.Framework.Application.Decorators
{
    public class CommandHandlerCommitDecorator<T> : ICommandHandler<T>
        where T : ICommand
    {
        private readonly ICommandHandler<T> commandHandler;

        private readonly IUnitOfWork unitOfWork;

        public CommandHandlerCommitDecorator(ICommandHandler<T> commandHandler, IUnitOfWork unitOfWork)
        {
            this.commandHandler = commandHandler;
            this.unitOfWork = unitOfWork;
        }

        public async Task Handle(T command, CancellationToken cancellationToken = default)
        {
            try
            {
                await unitOfWork.BeginTransaction(cancellationToken).ConfigureAwait(false);
                await commandHandler.Handle(command, cancellationToken).ConfigureAwait(false);
                await unitOfWork.CommitTransaction(cancellationToken).ConfigureAwait(false);
            }
            catch
            {
                await unitOfWork.RollbackTransaction(cancellationToken).ConfigureAwait(false);
                throw;
            }
        }
    }
}