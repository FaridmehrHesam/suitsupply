﻿using Autofac;
using SuitSupply.Framework.Application;
using SuitSupply.Framework.Application.Decorators;
using SuitSupply.Framework.Infrastructure;

namespace SuitSupply.ProductContext.Application.IoC
{
    public class ProductApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGenericDecorator(typeof(CommandHandlerLoggingDecorator<>), typeof(ICommandHandler<>));
            builder.RegisterGenericDecorator(typeof(CommandHandlerCommitDecorator<>), typeof(ICommandHandler<>));

            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(ICommandHandler<>))
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(IEventHandler<>))
                .InstancePerLifetimeScope();

            base.Load(builder);
        }
    }
}