﻿using System.Threading;
using System.Threading.Tasks;
using SuitSupply.Framework.Application;
using SuitSupply.Framework.Application.Exceptions;
using SuitSupply.ProductContext.Application.Contract.Products.Commands;
using SuitSupply.ProductContext.Domain.Products.Services;

namespace SuitSupply.ProductContext.Application.Products.CommandHandlers
{
    public class DeleteProductCommandHandler : ICommandHandler<DeleteProductCommand>
    {
        private readonly IProductRepository productRepository;

        public DeleteProductCommandHandler(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        public async Task Handle(DeleteProductCommand command, CancellationToken cancellationToken = default)
        {
            var product = await productRepository
                .GetByCode(command.Code, cancellationToken)
                .ConfigureAwait(false);

            if (product == null)
                throw new EntityNotFoundException();

            productRepository.Remove(product);
        }
    }
}