﻿using System.Threading;
using System.Threading.Tasks;
using SuitSupply.Framework.Application;
using SuitSupply.Framework.Application.Exceptions;
using SuitSupply.ProductContext.Application.Contract.Products.Commands;
using SuitSupply.ProductContext.Domain.Products.Services;

namespace SuitSupply.ProductContext.Application.Products.CommandHandlers
{
    public class ConfirmProductPriceCommandHandler : ICommandHandler<ConfirmProductPriceCommand>
    {
        private readonly IProductRepository productRepository;

        public ConfirmProductPriceCommandHandler(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        public async Task Handle(ConfirmProductPriceCommand command, CancellationToken cancellationToken = default)
        {
            var product = await productRepository
                .GetByCode(command.Code, cancellationToken)
                .ConfigureAwait(false);

            if (product == null)
                throw new EntityNotFoundException();

            product.ConfirmPrice();
        }
    }
}