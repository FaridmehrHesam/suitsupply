﻿using System.Threading;
using System.Threading.Tasks;
using SuitSupply.Framework.Application;
using SuitSupply.ProductContext.Application.Contract.Products.Commands;
using SuitSupply.ProductContext.Domain.Products;
using SuitSupply.ProductContext.Domain.Products.Services;
using SuitSupply.ProductContext.Domain.Products.ValueObjects;

namespace SuitSupply.ProductContext.Application.Products.CommandHandlers
{
    public class CreateProductCommandHandler : ICommandHandler<CreateProductCommand>
    {
        private readonly IProductRepository productRepository;

        public CreateProductCommandHandler(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        public Task Handle(CreateProductCommand command, CancellationToken cancellationToken = default)
        {
            var price = new Price(command.Price);
            var product = Product.Create(command.Code, command.Name, price);

            productRepository.Add(product);

            return Task.CompletedTask;
        }
    }
}