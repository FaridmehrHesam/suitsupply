﻿using System.Threading;
using System.Threading.Tasks;
using SuitSupply.Framework.Application;
using SuitSupply.Framework.Application.Exceptions;
using SuitSupply.ProductContext.Application.Contract.Products.Commands;
using SuitSupply.ProductContext.Domain.Products.Services;

namespace SuitSupply.ProductContext.Application.Products.CommandHandlers
{
    public class SetProductPhotoCommandHandler : ICommandHandler<SetProductPhotoCommand>
    {
        private readonly IProductRepository productRepository;

        public SetProductPhotoCommandHandler(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        public async Task Handle(SetProductPhotoCommand command, CancellationToken cancellationToken = default)
        {
            var product = await productRepository
                .GetByCode(command.Code, cancellationToken)
                .ConfigureAwait(false);

            if (product == null)
                throw new EntityNotFoundException();

            product.SetPhoto(command.Photo);
        }
    }
}