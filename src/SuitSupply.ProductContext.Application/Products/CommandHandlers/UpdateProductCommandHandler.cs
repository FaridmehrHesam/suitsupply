﻿using System.Threading;
using System.Threading.Tasks;
using SuitSupply.Framework.Application;
using SuitSupply.Framework.Application.Exceptions;
using SuitSupply.ProductContext.Application.Contract.Products.Commands;
using SuitSupply.ProductContext.Domain.Products.Services;
using SuitSupply.ProductContext.Domain.Products.ValueObjects;

namespace SuitSupply.ProductContext.Application.Products.CommandHandlers
{
    public class UpdateProductCommandHandler : ICommandHandler<UpdateProductCommand>
    {
        private readonly IProductRepository productRepository;

        public UpdateProductCommandHandler(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        public async Task Handle(UpdateProductCommand command, CancellationToken cancellationToken = default)
        {
            var product = await productRepository
                .GetByCode(command.CurrentCode, cancellationToken)
                .ConfigureAwait(false);

            if (product == null)
                throw new EntityNotFoundException();

            product.SetCode(command.Code);
            product.SetName(command.Name);
            product.SetPrice(new Price(command.Price));
        }
    }
}