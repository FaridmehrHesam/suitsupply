﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SuitSupply.Framework.Infrastructure;
using SuitSupply.ProductContext.Domain.Products.Events;

namespace SuitSupply.ProductContext.Application.Products.EventHandlers
{
    public class ProductCreatedEventHandler : IEventHandler<ProductCreatedEvent>
    {
        private readonly ILogger<ProductCreatedEventHandler> logger;

        public ProductCreatedEventHandler(ILogger<ProductCreatedEventHandler> logger)
        {
            this.logger = logger;
        }

        public Task Handle(ProductCreatedEvent domainEvent, CancellationToken cancellationToken = default)
        {
            //Just for showing :)
            logger.LogInformation("Product was created.");

            return Task.CompletedTask;
        }
    }
}