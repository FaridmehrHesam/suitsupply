﻿using System.Threading;
using System.Threading.Tasks;
using SuitSupply.Framework.Domain;

namespace SuitSupply.Framework.Infrastructure
{
    public interface IEventBus
    {
        Task Dispatch<T>(T domainEvent, CancellationToken cancellationToken = default) where T : IDomainEvent;
    }
}