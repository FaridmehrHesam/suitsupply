﻿using System.Threading;
using System.Threading.Tasks;

namespace SuitSupply.Framework.Infrastructure
{
    public interface IIntegrationEventBus
    {
        Task Dispatch<T>(T integrationEvent, CancellationToken cancellationToken = default) where T : IIntegrationEvent;
    }
}