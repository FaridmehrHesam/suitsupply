﻿using System.Threading;
using System.Threading.Tasks;

namespace SuitSupply.Framework.Infrastructure
{
    public interface IIntegrationEventHandler<in T> where T : IIntegrationEvent
    {
        Task Handle(T integrationEvent, CancellationToken cancellationToken = default);
    }
}