﻿using System;

namespace SuitSupply.Framework.Infrastructure
{
    public interface IIntegrationEvent
    {
        DateTime EventTime { get; }

        Guid EventId { get; }
    }
}