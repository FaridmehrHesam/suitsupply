﻿using System.Threading;
using System.Threading.Tasks;
using SuitSupply.Framework.Domain;

namespace SuitSupply.Framework.Infrastructure
{
    public interface IEventHandler<in T> where T : IDomainEvent
    {
        Task Handle(T domainEvent, CancellationToken cancellationToken = default);
    }
}