﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using SuitSupply.Framework.Domain;

namespace SuitSupply.Framework.Infrastructure
{
    public sealed class EventBus : IEventBus
    {
        private readonly IServiceProvider serviceProvider;

        public EventBus(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public Task Dispatch<T>(T domainEvent, CancellationToken cancellationToken = default)
            where T : IDomainEvent
        {
            var eventHandlerType = typeof(IEventHandler<>).MakeGenericType(domainEvent.GetType());
            var tasks = new List<Task>();

            dynamic eventHandlers = serviceProvider.GetServices(eventHandlerType);
            foreach (var eventHandler in eventHandlers)
                tasks.Add(eventHandler.Handle((dynamic)domainEvent, cancellationToken));

            return Task.WhenAll(tasks);
        }
    }
}