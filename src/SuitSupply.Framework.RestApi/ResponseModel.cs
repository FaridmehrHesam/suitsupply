﻿namespace SuitSupply.Framework.RestApi
{
    public class ResponseModel<T> where T : class
    {
        public T Values { get; set; }
    }
}