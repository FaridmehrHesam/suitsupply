using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using EntityFramework.Exceptions.SqlServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SuitSupply.Framework.Application;
using SuitSupply.Framework.Infrastructure;
using SuitSupply.Framework.ReadModel;
using SuitSupply.ProductContext.Application.IoC;
using SuitSupply.ProductContext.Facade.IoC;
using SuitSupply.ProductContext.Infrastructure;
using SuitSupply.ProductContext.Persistence;
using SuitSupply.ProductContext.Persistence.IoC;
using SuitSupply.ProductContext.ReadModel;
using SuitSupply.ProductContext.ReadModel.IoC;
using SuitSupply.ProductContext.RestApi.Services;
using SuitSupply.ProductContext.RestApi.v1_0;
using SuitSupply.ProductContext.RestApi.v1_0.Products.Controllers;

namespace SuitSupply.ProductContext.RestApi.Host
{
    public class Startup
    {
        private readonly IConfiguration configuration;

        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var containerBuilder = new ContainerBuilder();
            var connectionString = configuration.GetConnectionString("DefaultConnection");

            services.AddCors(
                o => o.AddPolicy(
                    "CorsPolicy",
                    builder => { builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader(); }));

            services.AddMvc()
                .AddMvcOptions(i => i.EnableEndpointRouting = false)
                .SetCompatibilityVersion(CompatibilityVersion.Latest)
                .AddApplicationPart(typeof(ProductsController).Assembly);

            services.AddHttpContextAccessor();
            services.AddVersionedApiExplorer();
            services.AddSwagger();
            services.AddLogging(configuration);
            services.AddApiVersioning(options => options.ReportApiVersions = true);
            //TODO: Read number of pool from configuration
            services.AddDbContextPool<ProductWriteDbContext>(
                i => { i.UseSqlServer(connectionString).UseExceptionProcessor(); }, 32);
            services.AddDbContextPool<ProductReadDbContext>(i => { i.UseSqlServer(connectionString); }, 32);
            services.AddScoped<ICommandBus, CommandBus>();
            services.AddScoped<IQueryBus, QueryBus>();
            services.AddScoped<IEventBus, EventBus>();
            services.AddScoped<ICsvExporter, CsvExporter>();
            services.AddSingleton<IImageStore, LocalImageStore>();
            services.Configure<ProductOptions>(configuration.GetSection(nameof(ProductOptions)));
            services.Configure<LocalImageStoreOptions>(configuration.GetSection(nameof(LocalImageStoreOptions)));

            containerBuilder.RegisterModule<ProductApplicationModule>();
            containerBuilder.RegisterModule<ProductPersistenceModule>();
            containerBuilder.RegisterModule<ProductReadModelModule>();
            containerBuilder.RegisterModule<ProductFacadeModule>();

            containerBuilder.Populate(services);

            var serviceProvider = new AutofacServiceProvider(containerBuilder.Build());

            return serviceProvider;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            IApiVersionDescriptionProvider apiVersionDescriptionProvider,
            ProductWriteDbContext productWriteDbContext)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();
            else app.UseGlobalException();


            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseSwagger(apiVersionDescriptionProvider);
            app.UseMvc();

            productWriteDbContext.Database.Migrate();
        }
    }
}