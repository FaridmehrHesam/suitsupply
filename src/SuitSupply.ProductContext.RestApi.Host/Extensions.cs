﻿using EntityFramework.Exceptions.Common;
using GlobalExceptionHandler.WebApi;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Serilog;
using SuitSupply.Framework.Application.Exceptions;
using SuitSupply.Framework.Domain.Exceptions;
using SuitSupply.ProductContext.RestApi.Host.Properties;

namespace SuitSupply.ProductContext.RestApi.Host
{
    public static class Extensions
    {
        public static void AddSwagger(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSwaggerGen(options =>
            {
                using (var serviceProvider = serviceCollection.BuildServiceProvider())
                {
                    var provider = serviceProvider.GetRequiredService<IApiVersionDescriptionProvider>();
                    foreach (var description in provider.ApiVersionDescriptions)
                        options.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description));
                }
            });
        }

        public static void UseSwagger(
            this IApplicationBuilder applicationBuilder,
            IApiVersionDescriptionProvider apiVersionDescriptionProvider)
        {
            applicationBuilder.UseSwagger();
            applicationBuilder.UseSwaggerUI(options =>
            {
                foreach (var description in apiVersionDescriptionProvider.ApiVersionDescriptions)
                    options.SwaggerEndpoint(
                        $"/swagger/{description.GroupName}/swagger.json",
                        description.GroupName.ToUpperInvariant());
            });
        }

        public static void UseGlobalException(this IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.UseGlobalExceptionHandler(
                i =>
                {
                    i.ContentType = "application/json";
                    i.ResponseBody(ex => JsonConvert.SerializeObject(new {errorMessage = Resources.AnErrorOccurred}));
                    i.Map<EntityNotFoundException>().ToStatusCode(StatusCodes.Status404NotFound);
                    i.Map<DomainException>().ToStatusCode(StatusCodes.Status400BadRequest).WithBody(
                        (ex, context) => JsonConvert.SerializeObject(new {errorMessage = ex.Message}));
                    i.Map<UniqueConstraintException>().ToStatusCode(StatusCodes.Status400BadRequest).WithBody(
                        (ex, context) =>
                            JsonConvert.SerializeObject(new {errorMessage = Resources.DuplicateValueIsProvided}));
                });
        }

        public static void AddLogging(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            var logger = new LoggerConfiguration().ReadFrom
                .Configuration(configuration)
                .CreateLogger();

            serviceCollection.AddLogging(i => i.AddSerilog(logger));
        }

        private static OpenApiInfo CreateInfoForApiVersion(ApiVersionDescription description)
        {
            var info = new OpenApiInfo
            {
                Title = $"Product API {description.ApiVersion}",
                Version = description.ApiVersion.ToString()
            };

            if (description.IsDeprecated) info.Description = "This API version has been deprecated.";

            return info;
        }
    }
}