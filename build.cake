#addin "nuget:?package=Cake.Sonar&version=1.1.22"
#tool "nuget:?package=GitVersion.CommandLine&version=5.1.3"
#tool "nuget:?package=MSBuild.SonarQube.Runner.Tool&version=4.6.0"
#tool "nuget:?package=JetBrains.dotCover.CommandLineTools&version=2019.3.1"

var target = Argument("target", "Build");
var configuration = Argument("configuration", "Release");

var isOnWidnows = IsRunningOnWindows();
var sonarProjectName = "SuitSupply";
var sonarUrl = EnvironmentVariable("SONAR_URL");
var sonarUser = EnvironmentVariable("SONAR_USER");
var sonarPassword = EnvironmentVariable("SONAR_PASSWORD");

DotNetCoreMSBuildSettings msBuildSettings = null;
GitVersion gitVersion = null;

Setup(context =>
{	
    if (!isOnWidnows) return;
    
    gitVersion = context.GitVersion();
    msBuildSettings = GetMsBuildSettings();

	if(gitVersion.BranchName != "master")
		sonarProjectName += $":{gitVersion.BranchName.Replace("/", "-")}";

	if(target == "InspectWithIntegrationTest")
		sonarProjectName += "-Integration";    
});

Task("Clean-Artifacts")
	.Does(() => {

		if(DirectoryExists(Constants.ArtifactsDirectoryPath))
			DeleteDirectory(
			  Constants.ArtifactsDirectoryPath,
				new DeleteDirectorySettings { Recursive = true}
			  );

		EnsureDirectoryExists(Constants.ArtifactsDirectoryPath);
	});

Task("Sonar-Begin")
	.WithCriteria(isOnWidnows)
	.Does(() => {
	
		SonarBegin(new SonarBeginSettings {
			Name = sonarProjectName,
			Key = sonarProjectName,
			Version = $"{gitVersion.AssemblySemVer}{gitVersion.PreReleaseTagWithDash}",
			Url = sonarUrl,
			Login = sonarUser,
			Password = sonarPassword,
			VsTestReportsPath = $"{Constants.VsTestReportsDirectoryPath}/*.{Constants.VsTestLogger}",
			DotCoverReportsPath = Constants.DotCoverHtmlFilePath.FullPath,
			Exclusions = "**/Migrations/*.*"
		});
	});

Task("Sonar-End")
	.WithCriteria(isOnWidnows)
	.Does(() => {

		SonarEnd(new SonarEndSettings { Login = sonarUser, Password = sonarPassword });
  });

Task("Restore")
	.Does(() => {
	
        DotNetCoreRestore(Constants.SolutionFilePath.FullPath);
    });

Task("Build")
	.IsDependentOn("Restore")
	.Does(() => {
	
		DotNetCoreBuild(
			Constants.SolutionFilePath.FullPath,
				new DotNetCoreBuildSettings()
				{
					Configuration = configuration,
					MSBuildSettings = msBuildSettings,
					NoRestore = true
				});
	});

Task("UnitTest")
	.IsDependentOn("Clean-Artifacts")
	.IsDependentOn("Build")
	.Does(() => {

		var projects = GetFiles("./test/**/*UnitTests.csproj");
		if(projects.Count == 0)
		{
			Warning("No test project found.");
			return;
		}

		if(isOnWidnows)
		{
			var dotCoverCoverSettings = new DotCoverCoverSettings()
				.WithFilter("+:SuitSupply.*")
				.WithFilter("-:*ConvensionTests")
				.WithFilter("-:*UnitTests")
				.WithFilter("-:*IntegrationTests");

			DotCoverCover(tool => {
				tool.DotNetCoreTest(
					System.IO.Path.GetFullPath(Constants.UnitTestsSolutionFilePath.FullPath),
					new DotNetCoreTestSettings()
					{
						Configuration = configuration,
						Logger = Constants.VsTestLogger,
						ResultsDirectory = Constants.VsTestReportsDirectoryPath,
						NoRestore = true,
						NoBuild = true
					});
			  },
			  Constants.DotCoverCoverageFilePath,
			  dotCoverCoverSettings);

			DotCoverReport(
				Constants.DotCoverCoverageFilePath.FullPath,
				Constants.DotCoverHtmlFilePath.FullPath,
				new DotCoverReportSettings {
					ReportType = DotCoverReportType.HTML
				});
		}
		else
		{
			DotNetCoreTest(
			  Constants.UnitTestsSolutionFilePath.FullPath,
			  new DotNetCoreTestSettings()
			  {
				Configuration = configuration,
				Logger = Constants.VsTestLogger,
				ResultsDirectory = Constants.VsTestReportsDirectoryPath,
				NoRestore = true,
				NoBuild = true
			  });
		}
	});

Task("IntegrationTest")
	.IsDependentOn("Clean-Artifacts")
	.IsDependentOn("Build")
	.Does(() => {

		var projects = GetFiles("./test/**/*IntegrationTests.csproj");
		if(projects.Count == 0)
		{
			Warning("No integration test project found.");
			return;
		}

		if(isOnWidnows)
		{
			var dotCoverCoverSettings = new DotCoverCoverSettings()
				.WithFilter("+:SuitSupply.*")
				.WithFilter("-:*ConvensionTests")
				.WithFilter("-:*UnitTests")
				.WithFilter("-:*IntegrationTests");

			DotCoverCover(tool => {
				tool.DotNetCoreTest(
					System.IO.Path.GetFullPath(Constants.IntegrationTestsSolutionFilePath.FullPath),
					new DotNetCoreTestSettings()
					{
						Configuration = configuration,
						Logger = Constants.VsTestLogger,
						ResultsDirectory = Constants.VsTestReportsDirectoryPath,
						NoRestore = true,
						NoBuild = true
					});
			  },
			  Constants.DotCoverCoverageFilePath,
			  dotCoverCoverSettings);

			DotCoverReport(
				Constants.DotCoverCoverageFilePath.FullPath,
				Constants.DotCoverHtmlFilePath.FullPath,
				new DotCoverReportSettings {
					ReportType = DotCoverReportType.HTML
				});
		}
		else
		{
			DotNetCoreTest(
			  Constants.IntegrationTestsSolutionFilePath.FullPath,
			  new DotNetCoreTestSettings()
			  {
				Configuration = configuration,
				Logger = Constants.VsTestLogger,
				ResultsDirectory = Constants.VsTestReportsDirectoryPath,
				NoRestore = true,
				NoBuild = true
			  });
		}
	});

Task("InspectWithUnitTest")
	.IsDependentOn("Sonar-Begin")
	.IsDependentOn("UnitTest")
	.IsDependentOn("Sonar-End")
	.Does(() => {});

Task("InspectWithIntegrationTest")
	.IsDependentOn("Sonar-Begin")
	.IsDependentOn("IntegrationTest")
	.IsDependentOn("Sonar-End")
	.Does(() => {});

Task("Nuget-Package")
	.IsDependentOn("Clean-Artifacts")
	.IsDependentOn("Build")
	.Does(() => {

		var dotNetCorePackSettings = new DotNetCorePackSettings
		{
			Configuration = configuration,
			MSBuildSettings = msBuildSettings,
			OutputDirectory = Constants.NugetPackagesDirectoryPath,
			NoRestore = true,
			NoBuild = true
		};

		var projects = GetFiles("./src/**/*.csproj");
        foreach(var project in projects)
			DotNetCorePack(project.FullPath, dotNetCorePackSettings);
	});

Task("Publish")
	.Does(() => {
		
		DotNetCorePublish(
		  Constants.RestApiHostProjectFilePath.FullPath, 
		  new DotNetCorePublishSettings
		  {
			Configuration = configuration,
			MSBuildSettings = msBuildSettings,
			OutputDirectory = $"{Constants.PublishDirectoryPath}"
		});
	});

Task("Package")
	.IsDependentOn("Clean-Artifacts")
	.IsDependentOn("Publish")
	.Does(() => {
	
		var filePath = $"{Constants.ArtifactsDirectoryPath}/{gitVersion.AssemblySemVer}{gitVersion.PreReleaseTagWithDash}.zip";
		Zip($"{Constants.PublishDirectoryPath}", filePath);
	});

RunTarget(target);

// Helpers
public static class Constants
{
	public static DirectoryPath ArtifactsDirectoryPath => "artifacts";

	public static DirectoryPath PublishDirectoryPath => $"{ArtifactsDirectoryPath}/publish";

	public static DirectoryPath NugetPackagesDirectoryPath => "{ArtifactsDirectoryPath}/NugetPackages";

	public static DirectoryPath VsTestReportsDirectoryPath => $"{Constants.ArtifactsDirectoryPath}/VsTestReports";

	public static DirectoryPath DotCoverReportsDirectoryPath => $"{Constants.ArtifactsDirectoryPath}/DotCoverReports";

	public static FilePath SolutionFilePath => "SuitSupply.sln";

	public static FilePath UnitTestsSolutionFilePath => "SuitSupply.UnitTests.sln";

	public static FilePath IntegrationTestsSolutionFilePath => "SuitSupply.IntegrationTests.sln";

	public static FilePath RestApiHostProjectFilePath => "src/SuitSupply.ProductContext.RestApi.Host/SuitSupply.ProductContext.RestApi.Host.csproj";

	public static FilePath DotCoverCoverageFilePath => $"{Constants.DotCoverReportsDirectoryPath}/Coverage.dcvr";

	public static FilePath DotCoverHtmlFilePath => $"{Constants.DotCoverReportsDirectoryPath}/Report.html";
	
	public static string VsTestLogger = "trx";
}

private DotNetCoreMSBuildSettings GetMsBuildSettings()
{
    var settings = new DotNetCoreMSBuildSettings();

    settings.WithProperty("AssemblyVersion", gitVersion.AssemblySemVer);
    settings.WithProperty("VersionPrefix", gitVersion.AssemblySemVer);
    settings.WithProperty("FileVersion", gitVersion.AssemblySemVer);
    settings.WithProperty("InformationalVersion", $"{gitVersion.AssemblySemVer}{gitVersion.PreReleaseTagWithDash}");
    settings.WithProperty("VersionSuffix", gitVersion.PreReleaseLabel);

    return settings;
}