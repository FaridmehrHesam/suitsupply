# SuitSupply

## Back-End Considerations: 

```sh
 1. Install .Net Core 3.1
 2. Photo can only be type of JPEG
 3. Correct database connenction string must be provided in appsettings.json
 4. cd src/SuitSupply.ProductContext.RestApi.Host
 5. dotnet run
 6. browse http://localhost:5000/swagger
```
## Front-End Considerations: 

``` sh
 1. Install Node.js
 2. Install Yarn
 3. Add vue cli using "yarn global add @vue/cli"
 4. cd src/SuitSupply.Spa
 5. yarn install
 6. yarn serve
 7. browse http://localhost:8080
```